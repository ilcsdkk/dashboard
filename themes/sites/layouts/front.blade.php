<!DOCTYPE html>
<!--                                     ____
                                         |  |
       _____  _  ___  ________  ______   |  |__     __   __
     /  ___/ | ' __/ |  ___  | /  _____\ |   _  \  |  | |  |
    |  |__   |  |    | |___| | \_____  \ |  |_)  | |  |_|  |
     \_____\ |__|    |_______| /_______/ |_'____/  _\___,  |
      https://github.com/alfredcrosby/ilearn      |_______/
 -->
<html class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NTMC Dashboard</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700, 900" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{  asset('assets/theme/adminLTE/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="{{ asset('assets/stylesheets/login.min.css') }}" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body class="login-page">
  <div class="site">

    <div class="copyright">&copy; ILCS 2016.</div>
  </div>

  @yield('content')

  <div class="modal fade" id="resetModal" tabindex="-1" role="dialog" aria-labelledby="resetModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Reset Password</h4>
        </div>
        <div class="modal-body clearfix">
          @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
          @endif

        </div>
      </div>
    </div>
  </div>
  <script src="{{  asset('assets/theme/adminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript">
    (function($){
      $('#loginModal').modal({ show: true });
    })(jQuery);
  </script>
</body>
</html>
