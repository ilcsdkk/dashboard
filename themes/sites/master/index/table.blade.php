<table class="{{ $class or 'table' }} datatable">
    @if(count($columns))
	<thead>
		<tr>
            <th> No. </th>
        @foreach($columns as $c)
            <th {!! $c->getClasses() ? ' class="' . $c->getClassString() . '"' : '' !!}>
                @if($c->isSortable())
                    <a href="{{ $c->getSortURL() }}">
                        {!! $c->getLabel() !!}
                        @if($c->isSorted())
                            @if($c->getDirection() == 'asc')
                                <span class="fa fa-sort-asc"></span>
                            @elseif($c->getDirection() == 'desc')
                                <span class="fa fa-sort-desc"></span>
                            @endif
                        @endif
                    </a>
                @else
                    {{ $c->getLabel() }}
                @endif
            </th>
        @endforeach

		</tr>
	</thead>
    @endif
	<tbody>
        @if(count($rows))
            @foreach($rows as $k => $r)
        <tr>
            <td> {{ $k+1 }} </td>
            @foreach($columns as $c)
                <td {!! $c->getClasses() ? ' class="' . $c->getClassString() . '"' : '' !!}>
                    @if($c->hasRenderer())
                    {{-- Custom renderer applied to this column, call it now --}}
                    {!! $c->render($r) !!}
                    @else
                    {{-- Use the "rendered_foo" field, if available, else use the plain "foo" field --}}
                        {!! $r->{'rendered_' . $c->getField()} or $r->{$c->getField()} !!}
                    @endif
                </td>
            @endforeach

            <td><a href="{{ route('master.table.action',[$table, $r->$primary_key,'edit'])  }}" class="action-table">Edit</a> 
                <a href="{{ route('master.table.delete',[$table,$r->$primary_key])  }}" class="action-table">Delete</a>
                <a href="{{ route('master.table.action',[$table,$r->$primary_key,'read'])  }}" class="action-table">Detail</a>
            </td>
        </tr>

            @endforeach
        @endif
    </tbody>
</table>



@if(is_object($rows) && class_basename(get_class($rows)) == 'LengthAwarePaginator')
    {{-- Collection is paginated, so render that --}}
    {!! $rows->render() !!}
@endif
<style type="text/css">
    .action-table {padding-right: 5px}
</style>