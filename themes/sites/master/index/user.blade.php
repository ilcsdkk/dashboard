	{!!
    $data->columns(array(
        'id' 			=> 'User id',
        'first_name' 	=> 'Name',
        'email' 		=> 'Email',
        'role'			=> 'Role',
        'action'		=> 'Action',
    ))
    ->modify('role', function($data){
    	return ($data->role !== null)? $data->role->name : 'Null' ;
    })
    ->modify('action', function($data){
    	return '<a href="'.url('admin/master/table/user/edit/'.$data->id).'" class=""> Edit </a> <a href="'.url('admin/master/table/user/delete/'.$data->id).'" > Delete </a>';
    })
    ->render() 
!!}