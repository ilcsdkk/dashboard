@extends('sites.layouts.backend')

@section('title', 'List Data '.ucfirst($table))


@section('content')

      <h1>
        List Data
        <small>{{$table}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12 connectedSortable">


          <!-- solid sales graph -->
          <div class="box box-solid">
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">{{$table}}</h3>

              <div class="box-tools pull-right">
                <a href="{{ route('master.table.create',[ $table ])}}" type="button" class="btn bg-teal btn-sm"><i class="fa fa-plus"></i> Add Data </a>
              </div>
            </div>
            <div class="box-body border-radius-none">
            {!!
                $html_table->render()
            !!}
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-border">
              <div class="row">
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="{{ count($data) }}" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label">All Data</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                  <input type="text" class="knob" data-readonly="true" value="50" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label">Data Active</div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4 text-center">
                  <input type="text" class="knob" data-readonly="true" value="30" data-width="60" data-height="60" data-fgColor="#39CCCC">

                  <div class="knob-label">Data Delete</div>
                </div>
                <!-- ./col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
 @stop
