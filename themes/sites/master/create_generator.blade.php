@extends('sites.layouts.backend')

@section('title', 'Adding Data '.ucfirst($table))


@section('content')
<!-- Horizontal Form -->
<div class="box box-info">
<div class="box-header with-border">
  <h3 class="box-title">Form {{ $table }}</h3>
</div>
<!-- /.box-header -->
<!-- form start -->
{!! Form::open(['route' => ['master.table.post',$table],'method' => 'post']) !!}
<form class="form-horizontal">
  <div class="box-body">
@if(isset($data))
  {{ Form::hidden('id', @$data->getKey()) }}
@endif

@foreach($masters as $master)

  @if(isset($relation) && $master == $relation)
    <div class="form-group">
      <label for="input" class="col-sm-2 control-label">{{ str_ucfrist($master) }}</label>
            <div class="col-sm-10">
            <select class="form-control" name="{{ $relation }}">
              <option value="NULL">{{ ($relation == $table . 'parent_id')? 'Parent' : 'Choose one' }}</option>
              {{-- select option from parent --}}
            @foreach( $relations as $show )
              @if (!$show->{$relation})
                  <option value="{{ $show->{$relation} }}">{{ (!empty($show->langs))? $show->langs[0]->{$relation_name} : $show->{$relation_name} }}</option>
                @endif
              @endforeach
            </select>
        </div>
    </div>
  @elseif (!in_array($master, $hidden))
    <div class="form-group">
      <label for="input" class="col-sm-2 control-label">{{ str_ucfrist($master) }}</label>
      <div class="col-sm-10">
      {!! Form::{getFormType($table,$master)}($master,@$data->{$master},['class' => 'form-control','placeholder' => str_ucfrist($master), (@$readonly)? 'readonly' : '' ]) !!}
      </div>
    </div>
  @elseif($master == $table."_status")
    <div class="form-group">
      <label for="input" class="col-sm-2 control-label">{{ str_ucfrist($master) }}</label>
            <div class="col-sm-10">
            <select class="form-control">
              <option value="1">Active</option>
              <option value="2">Inactive</option>
            </select>
        </div>
    </div>
  @endif
@endforeach
@if (isset($langs))
  @foreach($langs as $k => $lang)

    <h3 class="box-title" style="text-align: center;">{{ $lang->language_name }}</h3>
    @foreach($masterLangs as $masterLang)
      @if (!in_array($masterLang, $hidden))
        @if ( $masterLang == $table.'_lang_id')
          {!! Form::hidden("$masterLang"."[]", (@$data->langs[$k]->language_id)? @$data->langs[$k]->language_id : @$lang->language_id) !!}
        @else
            <div class="form-group">
              <label for="input" class="col-sm-2 control-label">{{ str_ucfrist($masterLang) }}</label>
              <div class="col-sm-10">
            {!! Form::{getFormType($table.'_lang',$masterLang)}($masterLang."[]",@$data->langs[$k]->$masterLang,['class' => 'form-control','placeholder' => str_ucfrist($masterLang), (@$readonly)? 'readonly' : '' ]) !!}
              </div>
            </div>
        @endif
      @elseif($masterLang == $table."_status")
        <div class="form-group">
              <label for="input" class="col-sm-2 control-label">{{ str_ucfrist($masterLang) }}</label>
                <div class="col-sm-10">
                <select class="form-control">
                  <option value="1">Active</option>
                  <option value="2">Inactive</option>
                </select>
            </div>
        </div>
      @endif
    @endforeach

  @endforeach
@endif

@if (!@$readonly)
  <!-- /.box-body -->
  <div class="box-footer">
    <a href="{{ url('admin') }}" class="btn btn-default">Cancel</a>
    <button type="submit" class="btn btn-info pull-right">Save</button>
  </div>
@endif
  <!-- /.box-footer -->
{!! Form::close() !!}
</div>
@stop
