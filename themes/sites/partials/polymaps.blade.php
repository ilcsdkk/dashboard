@section('styles')
<style type="text/css">
  #map {
    width: 100%;
    height:  450px;
    margin: 0;
    padding: 0;
  }
</style>
@stop

<div >
  <div id="map"></div>
</div>

@section('custom_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG808PBL5_qAnsrpgzPHIQQX0L8XGFDFE&libraries=drawing">
</script>
<script type="text/javascript">
  var map;
  function initMap() {
    var myLatLng =new google.maps.LatLng('-02.2000', '118.9288');  //{lat: '-02.2000', lng: '120.9288'};
     map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 5
    });

    var drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.MARKER,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        // drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
        drawingModes: ['polygon']

      },
      markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
      circleOptions: {
        fillColor: '#ffff00',
        fillOpacity: 1,
        strokeWeight: 5,
        clickable: false,
        editable: true,
        zIndex: 1
      }
    });
    drawingManager.setMap(map);
  }
  $(function () {
    initMap();
  });
</script>
@stop
