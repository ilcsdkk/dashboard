@section('styles')
<style type="text/css">
#map {
  width: 100%;
  height:  450px;
  margin: 0;
  padding: 0;
}
</style>
@stop

<div style="width:100%; height:100%">
  <div id="map"></div>
</div>

@section('custom_scripts')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
  var map;
  function initMap() {
    var myLatLng =new google.maps.LatLng('-02.2000', '118.9288');  //{lat: '-02.2000', lng: '120.9288'};

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: myLatLng
    });
  }

  $(function () {
    initMap();
  });
</script>
@stop
