@section('styles')
  <style type="text/css">
    #map {
      width: 100%;
      height:  450px;
      margin: 0;
      padding: 0;
    }
  </style>
@stop

<div id="map"></div>

<div id="timeline-item-template" style="display:none" class="timeline-item">
  <div class="timeline-body">
    <div class="timeline-body-arrow"></div>
    <div class="timeline-body-head">
      <div class="timeline-body-head-caption">
        <a href="#" class="timeline-body-title font-blue-madison">{DRIVER_NAME}</a>
        <span class="timeline-body-time font-grey-cascade">Posted at {TIMELINE_TIME}</span>
      </div>
    </div>
    <div class="timeline-body-content">
      <span class="font-grey-cascade">{MESSAGE}</span>
    </div>
  </div>
</div>

<div id="info-template" style="display: none">
  <h4>Truck Information</h4>
  <p>Id: {id}</p>
  <p>Supir: Jaja</p>
</div>

<div id="alert-template" style="display: none">
  <h4>Alert</h4>
  <p>Driver: {driverName}</p>
  <p>Phone: {driverPhone}</p>
  <p>Message: {message}</p>
  <p>Created: {created}</p>
</div>

@section('custom_scripts')
  <script src="{{ asset('assets/javascripts/socket.io.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG808PBL5_qAnsrpgzPHIQQX0L8XGFDFE&libraries=drawing"></script>

  <script type="text/javascript">
    var TruckLocationController = function(){
      this.getTruckLocationApi = '{{ url('/dashboard_api/v1/truck_location') }}';
    }

    TruckLocationController.prototype.get = function(bound, callback){
      var params = {
        Longitude1: bound.lngSouthWest, Latitude1: bound.latSouthWest, 
        Longitude2: bound.lngNorthEast, Latitude2: bound.latNorthEast
      };

      $.get(this.getTruckLocationApi, params, function(data){
        callback(data.payload.Datasource);
      }, 'json');
    }

    var PanicReportController = function(){
      this.getPanicReportApi = '{{ url('/dashboard_api/v1/panic_report') }}';
      
    }

    PanicReportController.prototype.get = function(bound, callback){
      var params = {
        Longitude1: bound.lngSouthWest, Latitude1: bound.latSouthWest, 
        Longitude2: bound.lngNorthEast, Latitude2: bound.latNorthEast
      };

      $.get(this.getPanicReportApi, params, function(data){
        callback(data.payload.Datasource);
      }, 'json');
    }

    var PanicTimeline = function(){
      this.containerElement = '#panic-timeline';
      this.timeline = $(this.containerElement);
    }

    PanicTimeline.prototype.redrawPanicReports = function(datasource){
      console.log(datasource);
      console.log('Redraw Panic Report');

      var _this = this;
      this.timeline.html('');

      datasource.forEach(function(data, i){
        var elemStr = $('#timeline-item-template').clone().removeAttr('id').html();
        elemStr = elemStr.replace('{DRIVER_NAME}', data.driverName);
        elemStr = elemStr.replace('{TIMELINE_TIME}', data.created);
        elemStr = elemStr.replace('{MESSAGE}', data.message);

        $(_this.timeline).append(elemStr);
      });
    }

    var NtmcMap = function(config){
      // Google maps object
      this.map = null;

      // Target div element for map
      this.containerElement = 'map';
      // Jakarta Long Lat
      this.defaultCenter = new google.maps.LatLng('-6.174752', '106.827201');

      // Markers
      this.truckLocationMarkers = [];
      this.panicReportMarkers = [];
      this.posPolisiMarkers = [];

      this.currentBound = {
        latNorthEast : 0,
        lngNorthEast : 0,
        latSouthWest : 0,
        lngSouthWest : 0,
      }

      this.dataRefresh = typeof(config.dataRefresh) !== 'undefined' ? config.dataRefresh : function(){};
    }

    NtmcMap.prototype.panicReportMarkerClick = function(){
      console.log('Panic Report Marker Click');
      
      var infoHtml = $('#alert-template').html();

      //Replace template holders with values
      var payload = this.get('payload');
      for(var el in payload){
        infoHtml = infoHtml.replace("{" + el + "}", payload[el]);
      }

      var infowindow = new google.maps.InfoWindow();
      infowindow.setContent(infoHtml);
      infowindow.open(map, this);
    }

    NtmcMap.prototype.truckLocationMarkerClick = function(){
      console.log('Truck Location Marker Click');

      var infoHtml = $('#info-template').html();

      //Replace template holders with values
      var payload = this.get('payload');
      for(var el in payload){
        infoHtml = infoHtml.replace("{" + el + "}", payload[el]);
      }

      var infowindow = new google.maps.InfoWindow();
      infowindow.setContent(infoHtml);
      infowindow.open(map, this);
    }

    NtmcMap.prototype.redrawPanicReports = function(datasource){
      console.log(datasource);
      console.log('Redraw Panic Report');

      var _this = this;

      // remove all markers
      this.panicReportMarkers = [];
      this.panicReportMarkers.forEach(function(marker, i) {
        marker.setMap(null);
      });

      // Place marker
      datasource.forEach(function(data, i){
        var marker = new google.maps.Marker({position: data.markerLatLng, map: _this.map});
        marker.set("payload", data);
        google.maps.event.addListener(marker, 'click', _this.panicReportMarkerClick);

        _this.panicReportMarkers.push(marker);
      });      
    }

    NtmcMap.prototype.redrawTruckLocations = function(datasource){
      console.log(datasource);
      console.log('Redraw Tuck Locations');

      var _this = this;

      // remove all markers
      this.truckLocationMarkers = [];
      this.truckLocationMarkers.forEach(function(marker, i) {
        marker.setMap(null);
      });

      // Place marker
      datasource.forEach(function(data, i){
        var marker = new google.maps.Marker({position: data.markerLatLng, map: _this.map});
        marker.set("payload", data);
        google.maps.event.addListener(marker, 'click', _this.truckLocationMarkerClick);

        _this.truckLocationMarkers.push(marker);
      });      
    }

    NtmcMap.prototype.getTrucksInCircle = function(callback){
      var _this = this;

      google.maps.event.addListener(this.map, 'click', function(e){
        // Make sure no more click!
        google.maps.event.clearListeners(this, 'click');

        var trucks = [];
    
        var circle = new google.maps.Circle({
          center: e.latLng,
          clickable: true,
          fillColor: '#004de8',
          fillOpacity: 0.27,
          map: this,
          radius: 500,
          strokeColor: '#004de8',
          strokeOpacity: 0.62,
          strokeWeight: 1
        });
        var bounds = circle.getBounds();

        // Find trucks inside circle
        _this.truckLocationMarkers.forEach(function(marker, i) {
          if ( bounds.contains(marker.position) ) {
            trucks.push(marker.get('payload'));
          }
        });

        // Remove circle after 10s or click
        setTimeout(function(){
          circle.setMap(null);
        }, 10000);

        google.maps.event.addListener(circle, 'click', function(){
          this.setMap(null);
        });

        // Return back data to requestor
        var datasource = {
          longitude : e.latLng.lng(),
          latitude : e.latLng.lat(),
          trucks : trucks
        }

        callback(datasource);
      });
    }

    NtmcMap.prototype.init = function(){
      // Create the map
      var _this = this;
      this.map = new google.maps.Map(document.getElementById(this.containerElement), {
        zoom: 15,
        center: this.defaultCenter
      });

      // This listener will invoked after Zoom/Pan completed
      google.maps.event.addListener(this.map, 'idle', function(){
        console.log('Map On Idle');

        // Get & Store Current Map Bounds
        _this.currentBound.latNorthEast = this.getBounds().getNorthEast().lat();
        _this.currentBound.lngNorthEast = this.getBounds().getNorthEast().lng();
        _this.currentBound.latSouthWest = this.getBounds().getSouthWest().lat();
        _this.currentBound.lngSouthWest = this.getBounds().getSouthWest().lng();

        // Callback
        _this.dataRefresh(_this.currentBound);
      });
    }

    // Main Controller
    var DashboardController = function(){
      
      var dataRefresh = function(currentBound){
        // Truck Locations
        truckLocation.get(currentBound, function(rawData){
          // Remapping Properties
          var datasource = [];
          rawData.forEach(function(data, i){
            datasource.push({
              markerLatLng: new google.maps.LatLng(data.Latitude, data.Longitude), 
              id: data.TruckId,
              driverId: data.DriverId,
            });
          });

          // Draw On Map
          map.redrawTruckLocations(datasource);
        });

        // Panic Reports
        panicReport.get(currentBound, function(rawData){
          // Remapping Properties
          var datasource = [];
          rawData.forEach(function(data, i){
            datasource.push({
              markerLatLng: new google.maps.LatLng(data.Latitude, data.Longitude), 
              id: data.PanicReportId,
              driverName: data.DriverName,
              driverPhone: data.DriverPhone,
              message: data.Message,
              created: data.Created,
            });
          });

          // Draw on Map
          map.redrawPanicReports(datasource);

          // Draw on timeline
          timeline.redrawPanicReports(datasource);
        });
      }

      // Instantiate objects
      truckLocation = new TruckLocationController();
      panicReport = new PanicReportController();
      timeline = new PanicTimeline();
      map = new NtmcMap({
        // On map Zoom & Pan Change retrieve and redraw all data
        dataRefresh : dataRefresh
      });

      // Init Map
      map.init();

      // Periodic refresh
      setInterval(function(){
        dataRefresh(map.currentBound);
      }, 60000);

      // Other event listeners
      $('#submit-broadcast').click(function(){
        var message = $('#deskripsi-alert').val();
        if(message !== ''){
          alert('Please click area on map to select broadcast area');
          map.getTrucksInCircle(function(datasource){
            console.log(datasource);
            if(datasource.trucks.length){
              var driverIds = [];
              datasource.trucks.forEach(function(item, i){
                driverIds.push(item.driverId);
              });

              var param = {
                'message' : message, 
                'longitude' : datasource.longitude,
                'latitude' : datasource.latitude,
                'driver_ids' : driverIds
              }
              
              $.post('{{ url('/dashboard_api/v1/alert_notif') }}', param, function(data){
                $('#deskripsi-alert').val('');
                alert('Sent broadcast to ' + data.payload.SentCount + ' devices');
              }, 'json');
            }else{
              alert('No trucks found in target area. No broadcast sent');
            }
          });
        }else{
          alert('Please type the broadcast message to send');
        }
      });
    }

    $( document ).ready(function() {
      var dashboard = new DashboardController();
    });
  </script>
@stop