@extends('sites.layouts.master')

@section('content')
  <p id="power">0</p>
@stop

@section('footer')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <!--script src="https://cdn.socket.io/socket.io-1.3.5.js"></script-->
  <script src="{{ asset('assets/javascripts/socket.io.js') }}"></script>
  <script>
    var socket = io('http://192.168.10.10:3000');
    var socket = io('http://192.168.100.100:3000');
    console.log(socket);
    socket.on("test-channel:Ntmc\\Events\\EventName", function(message){
      // increase the power everytime we load test route
      console.log(message);
      $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
    });
  </script>
@stop