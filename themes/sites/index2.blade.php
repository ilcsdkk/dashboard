@extends('sites.layouts.backend')

@section('title', 'Dashboard')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tracking</li>
    </ol>
    <!--a href="{{-- route('users.create') --}}" class="btn btn-flat btn-info btn-xs"><i class="fa fa-plus"></i> Tambah Baru</a-->
@endsection

@section('content')
    <div class="row">
        <div class="pull-left col-xs-6 col-sm-8 col-md-8">

            <div class="box-body">
              <div class="form-horizontal">
                <div class="form-group col-lg-8">
                  {!! Form::textarea('deskripsi_alert', null, ['class'=>'form-control', 'id' => 'deskripsi-alert', 'placeholder' => 'Deskripsi Alert', 'style' => 'width:100%; height:34px; margin-top:6px;'] ) !!}
                </div>
                <div class="form-group col-lg-4">
                  <button id="submit-broadcast" class="btn btn-primary">Submit Broadcast</button>
                </div>
              </div>
            </div>
        </div>

        <div class="pull-right col-xs-6 col-sm-4 col-md-4">

            {!! Form::open(['role' => 'form', 'class' => 'form-horizontal', 'method' => 'get']) !!}
            <div class="box-body">
                <div class="form-group">
                    {!! Form::select('pos_polisi', \Ntmc\Models\PosPolisi::lists('nama', 'id')->toArray(), null, ['class'=>'form-control select2', 'id' => 'pos-polisi', 'placeholder' => 'Pilih Pos Polisi'] ) !!}
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            @include('sites.partials.circlemap')
        </div>

        <div class="col-lg-4">
            <div id="panic-timeline" class="" style="overflow-y: scroll; height: 450px">
                

            </div>
        </div>
    </div>
    <!-- <div id="map"></div> -->
    <!-- right col -->
@stop