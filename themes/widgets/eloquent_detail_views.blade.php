<table id="users-datatable" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Field</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($allColumns as $column)
        <tr>
            <td>{{ $column }}</td>
            <td>{{ $model->$column }}</td>
        </tr>
    @endforeach
    </tbody>
</table>