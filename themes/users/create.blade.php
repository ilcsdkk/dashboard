@extends('sites.layouts.backend')

@section('title', 'Dashboard')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah User</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Tambahkan User</h3>
            </div>

            {!! Form::open(['role' => 'form', 'class' => 'form-horizontal', 'route' => 'users.store']) !!}
                @include('users._form')
            {!! Form::close() !!}

        </div>
    </div>

    <div class="col-md-4">
        @include('users._box', ['users' => $users])
    </div>
</div>
@stop