@extends('sites.layouts.backend')

@section('title', 'Dashboard')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit User</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $user->username }}</h3>
            </div>

            {!! Form::model($user, ['route' => ['users.update', $user], 'method' =>'patch', 'role' => 'form', 'class' => 'form-horizontal']) !!}
                @include('users._form', ['model' => $user])
            {!! Form::close() !!}

        </div>
    </div>

    <div class="col-md-4">
        @include('users._box', ['users' => $users])
    </div>
</div>
@stop