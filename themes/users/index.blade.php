@extends('sites.layouts.backend')

@section('title', 'Dashboard')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">User List</li>
  </ol>
@endsection

@section('page_description')
  <a href="{{ route('users.create') }}" class="btn btn-flat btn-info btn-xs"><i class="fa fa-plus"></i> Tambah Baru</a>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tabel User</h3>
      </div>
      <div class="box-body">
        <table id="users-datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $idx => $user)
              <tr>
                <td>{{ ($idx+1) + ( ($users->currentPage() - 1) * $users->perPage() ) }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone_number }}</td>
                <td>
                  <div>
                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-flat btn-link btn-xs">Edit</a>
                    @if($user->username !== 'admin')
                      {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete', 'class' => 'form-delete-inline']) !!}
                      {!! Form::submit('Hapus', ['class'=>'btn btn-flat btn-link btn-link-danger btn-xs warning-delete', 'data-title' => $user->username]) !!}
                      {!! Form::close() !!}
                    @endif
                  </div>
                </td>
              </tr>
              @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>

        <div class="pull-right">
          @if( $querystring !== null )
            {!! $users->appends($querystring)->links() !!}
          @else
            {!! $users->links() !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop