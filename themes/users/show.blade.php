@extends('sites.layouts.backend')

@section('title', 'User')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">User Detail</li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Detail User</h3>
      </div>
      <div class="box-body">

        @widget('detailViews', ['model' => $user])

      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop