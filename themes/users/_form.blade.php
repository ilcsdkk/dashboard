<div class="box-body">
    <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
        {!! Form::label('username', 'Username', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('username', null, ['class'=> 'form-control', 'id' => 'username', 'autocomplete' => 'off']) !!}
            {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
        {!! Form::label('phone_number', 'No Telp', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('phone_number', null, ['class'=> 'form-control', 'id' => 'phone_number', 'autocomplete' => 'off']) !!}
            {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
        {!! Form::label('email', 'Email', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('email', null, ['class'=> 'form-control', 'id' => 'email', 'autocomplete' => 'off']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('password', 'Password', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::password('password', ['class' => 'form-control', 'autofocus' => 'autofocus'] ) !!}
            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
        {!! Form::label('role', 'Peran', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('role', ['staff' => 'Staff', 'admin' => 'Admin'], null, ['class'=>'form-control select2', 'id' => 'role', 'placeholder' => 'Pilih Peran'] ) !!}
            {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
        </div>
    </div>



    <div class="box-footer">
        <div class="col-md-offset-3">
            @if(Route::currentRouteName() == 'users.edit' )
                {!! Form::submit('Update', ['class'=>'btn btn-flat btn-success']) !!}
            @else
                {!! Form::submit('Add', ['class'=>'btn btn-flat btn-success']) !!}
            @endif
        </div>
    </div>
</div>