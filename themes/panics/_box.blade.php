<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Tipe Panik  Terbaru</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <ul class="products-list product-list-in-box">
            @forelse ($panics as $item)
                <li class="item">
                    <div class="product-info">
                        <a href="{{ route('panics.edit', $item) }}" class="product-title">
                            {{ $item->name }}
                        </a>
                    </div>
                </li>
            @empty
                <li class="item">
                    <div class="product-info">
                        Tidak ada tipe panik baru.
                    </div>
                </li>
            @endforelse
        </ul>
    </div>

    <div class="box-footer text-center">
        <a href="{{ route('panics.index') }}" class="uppercase">Lihat Semua Data</a>
    </div>
</div>