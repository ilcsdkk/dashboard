<div class="box-body">
    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {!! Form::label('name', 'Nama', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('name', null, ['class'=> 'form-control', 'id' => 'name', 'autocomplete' => 'off']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('deskripsi') ? 'has-error' : '' }}">
        {!! Form::label('deskripsi', 'Deskripsi', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('deskripsi', null, ['class'=> 'form-control', 'id' => 'deskripsi', 'autocomplete' => 'off']) !!}
            {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="box-footer">
        <div class="col-md-offset-3">
            @if(Route::currentRouteName() == 'panics.edit' )
                {!! Form::submit('Update', ['class'=>'btn btn-flat btn-success']) !!}
            @else
                {!! Form::submit('Add', ['class'=>'btn btn-flat btn-success']) !!}
            @endif
        </div>
    </div>
</div>