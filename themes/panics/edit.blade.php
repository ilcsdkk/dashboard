@extends('sites.layouts.backend')

@section('title', 'Tipe Panik')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('panics.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Tipe Panik</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $item->name }}</h3>
            </div>

            {!! Form::model($item, ['route' => ['panics.update', $item], 'method' =>'patch', 'role' => 'form', 'class' => 'form-horizontal']) !!}
                @include('panics._form', ['model' => $item])
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-md-4">
        @include('panics._box', ['panics' => $panics])
    </div>
</div>
@stop
