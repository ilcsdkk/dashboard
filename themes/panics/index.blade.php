@extends('sites.layouts.backend')

@section('title', 'Panic Types')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('panics.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">List Tipe Panik</li>
  </ol>
@endsection

@section('page_description')
  <a href="{{ route('panics.create') }}" class="btn btn-flat btn-info btn-xs"><i class="fa fa-plus"></i> Tambah Baru</a>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tipe Panik</h3>
      </div>
      <div class="box-body">
        <table id="users-datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Deskripsi</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($panics as $idx => $item)
              <tr>
                <td>{{ ($idx+1) + ( ($panics->currentPage() - 1) * $panics->perPage() ) }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->deskripsi }}</td>
                <td>
                  <div>
                    <a href="{{ route('panics.edit', $item->id) }}" class="btn btn-flat btn-link btn-xs">Edit</a>

                    {!! Form::open(['route' => ['panics.destroy', $item->id], 'method' => 'delete', 'class' => 'form-delete-inline']) !!}
                    {!! Form::submit('Hapus', ['class'=>'btn btn-flat btn-link btn-link-danger btn-xs warning-delete', 'data-title' => $item->nama]) !!}
                    {!! Form::close() !!}

                    <a href="{{ route('panics.show', $item->id) }}" class="btn btn-flat btn-link btn-xs">Detail</a>
                  </div>
                </td>
              </tr>
              @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Deskripsi</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>

        <div class="pull-right">
          @if( $querystring !== null )
            {!! $panics->appends($querystring)->links() !!}
          @else
            {!! $panics->links() !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop