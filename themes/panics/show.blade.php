@extends('sites.layouts.backend')

@section('title', 'Tipe Panik')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('panics.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Detail Tipe Panik</li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Detail Tipe Panik</h3>
      </div>
      <div class="box-body">

        @widget('detailViews', ['model' => $item])

      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop