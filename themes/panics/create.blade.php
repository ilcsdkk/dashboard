@extends('sites.layouts.backend')

@section('title', 'Tipe Panik')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('panics.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Tipe Panik</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Tambahkan Tipe Panik</h3>
            </div>

            {!! Form::open(['role' => 'form', 'class' => 'form-horizontal', 'route' => 'panics.store']) !!}
                @include('panics._form')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-md-4">
        @include('panics._box', ['panics' => $panics])
    </div>
</div>
@stop