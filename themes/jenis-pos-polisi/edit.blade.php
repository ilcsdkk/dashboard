@extends('sites.layouts.backend')

@section('title', 'Jenis Pos Polisi')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Jenis Pos Polisi</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $item->nama }}</h3>
            </div>

            {!! Form::model($item, ['route' => ['jenis-pos-polisi.update', $item], 'method' =>'patch', 'role' => 'form', 'class' => 'form-horizontal']) !!}
                @include('jenis-pos-polisi._form', ['model' => $item])
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-md-4">
        @include('jenis-pos-polisi._box', ['jenisPosPolisi' => $jenisPosPolisi])
    </div>
</div>
@stop
