<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Jenis Pos Polisi Terbaru</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <ul class="products-list product-list-in-box">
            @forelse ($jenisPosPolisi as $item)
                <li class="item">
                    <div class="product-info">
                        <a href="{{ route('jenis-pos-polisi.edit', $item) }}" class="product-title">
                            {{ $item->nama }}
                        </a>
                    </div>
                </li>
            @empty
                <li class="item">                            
                    <div class="product-info">
                        Tidak ada Jenis Pos Polisi baru.
                    </div>
                </li>
            @endforelse
        </ul>
    </div>

    <div class="box-footer text-center">
        <a href="{{ route('jenis-pos-polisi.index') }}" class="uppercase">Lihat Semua Data</a>
    </div>
</div>