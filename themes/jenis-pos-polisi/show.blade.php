@extends('sites.layouts.backend')

@section('title', 'Jenis Pos Polisi')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('jenis-pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Detail Jenis Pos Polisi</li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Detail Jenis Pos Polisi</h3>
      </div>
      <div class="box-body">

        @widget('detailViews', ['model' => $item])

      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop