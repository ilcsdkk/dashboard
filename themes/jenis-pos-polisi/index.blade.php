@extends('sites.layouts.backend')

@section('title', 'Pos Polisi')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('jenis-pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">List Jenis Pos Polisi</li>
  </ol>
@endsection

@section('page_description')
  <a href="{{ route('jenis-pos-polisi.create') }}" class="btn btn-flat btn-info btn-xs"><i class="fa fa-plus"></i> Tambah Baru</a>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Jenis Pos Polisi</h3>
      </div>
      <div class="box-body">
        <table id="users-datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($jenisPosPolisi as $idx => $jenis)
              <tr>
                <td>{{ ($idx+1) + ( ($jenisPosPolisi->currentPage() - 1) * $jenisPosPolisi->perPage() ) }}</td>
                <td>{{ $jenis->nama }}</td>
                <td>
                  <div>
                    <a href="{{ route('jenis-pos-polisi.edit', $jenis->id) }}" class="btn btn-flat btn-link btn-xs">Edit</a>

                    {!! Form::open(['route' => ['jenis-pos-polisi.destroy', $jenis->id], 'method' => 'delete', 'class' => 'form-delete-inline']) !!}
                    {!! Form::submit('Hapus', ['class'=>'btn btn-flat btn-link btn-link-danger btn-xs warning-delete', 'data-title' => $jenis->nama]) !!}
                    {!! Form::close() !!}

                    <a href="{{ route('jenis-pos-polisi.show', $jenis->id) }}" class="btn btn-flat btn-link btn-xs">Detail</a>
                  </div>
                </td>
              </tr>
              @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>

        <div class="pull-right">
          @if( $querystring !== null )
            {!! $jenisPosPolisi->appends($querystring)->links() !!}
          @else
            {!! $jenisPosPolisi->links() !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop