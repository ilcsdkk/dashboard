@extends('sites.layouts.backend')

@section('title', 'Jenis Pos Polisi')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('jenis-pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Jenis Pos Polisi</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Tambahkan Jenis Pos Polisi</h3>
            </div>

            {!! Form::open(['role' => 'form', 'class' => 'form-horizontal', 'route' => 'jenis-pos-polisi.store']) !!}
                @include('jenis-pos-polisi._form')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-md-4">
        @include('jenis-pos-polisi._box', ['jenisPosPolisi' => $jenisPosPolisi])
    </div>
</div>
@stop