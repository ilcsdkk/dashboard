<div class="box-body">
    <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
        {!! Form::label('nama', 'Nama', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('nama', null, ['class'=> 'form-control', 'id' => 'nama', 'autocomplete' => 'off']) !!}
            {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="box-footer">
        <div class="col-md-offset-3">
            @if(Route::currentRouteName() == 'jenis-pos-polisi.edit' )
                {!! Form::submit('Update', ['class'=>'btn btn-flat btn-success']) !!}
            @else
                {!! Form::submit('Add', ['class'=>'btn btn-flat btn-success']) !!}
            @endif
        </div>
    </div>
</div>