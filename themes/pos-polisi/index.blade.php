@extends('sites.layouts.backend')

@section('title', 'Pos Polisi')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">List Pos Polisi</li>
  </ol>
@endsection

@section('page_description')
  <a href="{{ route('pos-polisi.create') }}" class="btn btn-flat btn-info btn-xs"><i class="fa fa-plus"></i> Tambah Baru</a>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Pos Polisi</h3>
      </div>
      <div class="box-body">
        <table id="users-datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Lat</th>
              <th>Long</th>
              <th>Jenis Pos Polisi</th>
              <th>Contact Person</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($posPolisi as $idx => $pos)
              <tr>
                <td>{{ ($idx+1) + ( ($posPolisi->currentPage() - 1) * $posPolisi->perPage() ) }}</td>
                <td>{{ $pos->nama }}</td>
                <td>{{ $pos->lat }}</td>
                <td>{{ $pos->long }}</td>
                <td>{{ $pos->jenisPos->nama }}</td>
                <td>{{ $pos->contact_person }}</td>
                <td>
                  <div>
                    <a href="{{ route('pos-polisi.edit', $pos->id) }}" class="btn btn-flat btn-link btn-xs">Edit</a>

                    {!! Form::open(['route' => ['pos-polisi.destroy', $pos->id], 'method' => 'delete', 'class' => 'form-delete-inline']) !!}
                    {!! Form::submit('Hapus', ['class'=>'btn btn-flat btn-link btn-link-danger btn-xs warning-delete', 'data-title' => $pos->nama]) !!}
                    {!! Form::close() !!}

                    <a href="{{ route('pos-polisi.show', $pos->id) }}" class="btn btn-flat btn-link btn-xs">Detail</a>
                  </div>
                </td>
              </tr>
              @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Lat</th>
              <th>Long</th>
              <th>Jenis Pos Polisi</th>
              <th>Contact Person</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>

        <div class="pull-right">
          @if( $querystring !== null )
            {!! $posPolisi->appends($querystring)->links() !!}
          @else
            {!! $posPolisi->links() !!}
          @endif
        </div>
      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop