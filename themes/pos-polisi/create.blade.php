@extends('sites.layouts.backend')

@section('title', 'Pos Polisi')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tambah Pos Polisi</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Tambahkan Pos Polisi</h3>
            </div>

            {!! Form::open(['role' => 'form', 'class' => 'form-horizontal', 'route' => 'pos-polisi.store']) !!}
                @include('pos-polisi._form')
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-md-4">
        @include('pos-polisi._box', ['posPolisi' => $posPolisi])
    </div>
</div>
@stop