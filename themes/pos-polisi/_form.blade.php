<div class="box-body">
    <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
        {!! Form::label('nama', 'Nama', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('nama', null, ['class'=> 'form-control', 'id' => 'nama', 'autocomplete' => 'off']) !!}
            {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('lat') ? 'has-error' : '' }}">
        {!! Form::label('lat', 'Latitude', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('lat', null, ['class'=> 'form-control', 'id' => 'lat', 'autocomplete' => 'off']) !!}
            {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('long') ? 'has-error' : '' }}">
        {!! Form::label('long', 'Longitude', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('long', null, ['class'=> 'form-control', 'id' => 'long', 'autocomplete' => 'off']) !!}
            {!! $errors->first('long', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('jenis_pos_polisi_id') ? 'has-error' : '' }}">
        {!! Form::label('jenis_pos_polisi_id', 'Jenis Pos Polisi', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('jenis_pos_polisi_id', \Ntmc\Models\JenisPosPolisi::lists('nama', 'id')->toArray(), null, ['class'=>'form-control select2', 'id' => 'jenis_pos_polisi_id', 'placeholder' => 'Pilih Jenis Pos Polisi'] ) !!}
            {!! $errors->first('jenis_pos_polisi_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('parent') ? 'has-error' : '' }}">
        {!! Form::label('parent', 'Parent', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('parent', \Ntmc\Models\PosPolisi::lists('nama', 'id')->toArray(), null, ['class'=>'form-control select2', 'id' => 'parent', 'placeholder' => 'Pilih Pusat'] ) !!}
            {!! $errors->first('parent', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
        {!! Form::label('alamat', 'Alamat', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('alamat', null, ['class'=> 'form-control', 'id' => 'alamat', 'autocomplete' => 'off']) !!}
            {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('contact_person') ? 'has-error' : '' }}">
        {!! Form::label('contact_person', 'Contact Person', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('contact_person', null, ['class'=> 'form-control', 'id' => 'contact_person', 'autocomplete' => 'off']) !!}
            {!! $errors->first('contact_person', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="box-footer">
        <div class="col-md-offset-3">
            @if(Route::currentRouteName() == 'pos-polisi.edit' )
                {!! Form::submit('Update', ['class'=>'btn btn-flat btn-success']) !!}
            @else
                {!! Form::submit('Add', ['class'=>'btn btn-flat btn-success']) !!}
            @endif
        </div>
    </div>
</div>