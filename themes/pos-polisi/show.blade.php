@extends('sites.layouts.backend')

@section('title', 'Pos Polisi')

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="{{ route('pos-polisi.index') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Detail Pos Polisi</li>
  </ol>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Detail Pos Polisi</h3>
      </div>
      <div class="box-body">

        @widget('detailViews', ['model' => $pos])

      </div>
    </div>
  </div>
</div><!-- /.row -->
@stop