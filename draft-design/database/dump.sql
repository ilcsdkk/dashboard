-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: 192.168.10.10    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Trucks`
--

DROP TABLE IF EXISTS `Trucks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Trucks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_polisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Trucks`
--

LOCK TABLES `Trucks` WRITE;
/*!40000 ALTER TABLE `Trucks` DISABLE KEYS */;
/*!40000 ALTER TABLE `Trucks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_session`
--

DROP TABLE IF EXISTS `api_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_session` (
  `token` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `pin` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  `expired` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`token`),
  KEY `api_session_driver_id_index` (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_session`
--

LOCK TABLES `api_session` WRITE;
/*!40000 ALTER TABLE `api_session` DISABLE KEYS */;
INSERT INTO `api_session` VALUES ('57f70bae63574','23','0000000023805','2','8291',1,'2016-10-13 04:57:54','2016-10-07 02:42:54'),('57f7d0a37b42f','23','0000000023805','','7315',0,'2016-10-07 17:43:15','2016-10-07 16:43:15'),('57f7d0a37b60d','23','0000000023805','','7521',0,'2016-10-07 17:43:15','2016-10-07 16:43:15'),('57f7d0cba5479','23','0000000023805','','1792',1,'2016-10-07 17:44:31','2016-10-07 16:43:55'),('57f7d36841264','23','0000000023805','','9996',1,'2016-10-07 18:00:13','2016-10-07 16:55:04'),('57fa65824a116','23','0000000023805','','1453',1,'2016-10-09 17:01:59','2016-10-09 15:42:58'),('57fc235c36710','23','0000000023805','','3780',1,'2016-10-11 02:22:16','2016-10-10 23:25:16'),('57ff03559a33a','23','0000000023805','','3240',1,'2016-10-13 04:51:11','2016-10-13 03:45:25'),('57ff04f652558','23','0000000023805','','9429',1,'2016-10-13 05:44:29','2016-10-13 03:52:22'),('581b08034455e','23','0000000023805','','2797',1,'2016-11-03 10:52:14','2016-11-03 09:48:51'),('roech2','19','0000000023804','0','1000',1,'2016-10-06 02:14:25','2016-09-04 23:54:00'),('token','20','0000000019101','1','1111',1,'2016-10-06 02:13:26','2016-09-04 23:54:38');
/*!40000 ALTER TABLE `api_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `broadcast_message`
--

DROP TABLE IF EXISTS `broadcast_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `broadcast_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `broadcast_type_id` int(11) NOT NULL,
  `title` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `broadcast_range_meter` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `broadcast_message`
--

LOCK TABLES `broadcast_message` WRITE;
/*!40000 ALTER TABLE `broadcast_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `broadcast_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `current_driver_location`
--

DROP TABLE IF EXISTS `current_driver_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current_driver_location` (
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `heading` double(8,2) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `current_driver_location`
--

LOCK TABLES `current_driver_location` WRITE;
/*!40000 ALTER TABLE `current_driver_location` DISABLE KEYS */;
INSERT INTO `current_driver_location` VALUES ('19','0000000023804','0',106.840000,-6.180000,1.00,'2016-09-05 00:06:04'),('20','0000000019101','1',106.830000,-6.170000,1.00,'2016-09-05 00:08:41'),('23','0000000023805','',106.909208,-6.255139,1.00,'2016-11-03 09:52:14');
/*!40000 ALTER TABLE `current_driver_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driver_location`
--

DROP TABLE IF EXISTS `driver_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `heading` double NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver_location`
--

LOCK TABLES `driver_location` WRITE;
/*!40000 ALTER TABLE `driver_location` DISABLE KEYS */;
INSERT INTO `driver_location` VALUES (1,'19','0000000023805','',5.000000,5.000000,0,'2016-09-02 18:41:11'),(2,'19','0000000023805','',5.000000,5.000000,0,'2016-09-02 18:41:41'),(3,'19','0000000023805','',5.000000,5.000000,0,'2016-09-02 18:44:09'),(4,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:10:47'),(5,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:11:59'),(6,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:12:23'),(7,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:13:21'),(8,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:13:51'),(9,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:14:27'),(10,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:14:48'),(11,'23','0000000023805','',106.892720,-6.117620,0,'2016-10-07 03:15:13'),(12,'23','0000000023805','',106.892990,-6.117890,0,'2016-10-07 03:16:47'),(13,'23','0000000023805','',106.892990,-6.117890,0,'2016-10-07 03:17:25'),(14,'23','0000000023805','',106.892990,-6.117890,0,'2016-10-07 03:18:30'),(15,'23','0000000023805','',106.892990,-6.117890,0,'2016-10-07 03:19:31'),(16,'23','0000000023805','',106.892990,-6.117890,0,'2016-10-07 03:20:44'),(17,'23','0000000023805','',106.892990,-6.117890,0,'2016-10-07 03:21:53'),(18,'23','0000000023805','',106.893050,-6.117930,0,'2016-10-07 03:22:42'),(19,'23','0000000023805','',106.893050,-6.117930,0,'2016-10-07 03:23:08'),(20,'23','0000000023805','',106.893050,-6.117930,0,'2016-10-07 03:23:08'),(21,'23','0000000023805','',106.893050,-6.117930,0,'2016-10-07 03:24:02'),(22,'23','0000000023805','',106.893050,-6.117930,0,'2016-10-07 03:24:44'),(23,'23','0000000023805','',106.892710,-6.117560,0,'2016-10-07 03:26:41'),(24,'23','0000000023805','',106.892710,-6.117560,0,'2016-10-07 03:31:31'),(25,'23','0000000023805','',106.892990,-6.117950,0,'2016-10-07 03:32:36'),(26,'23','0000000023805','',106.892990,-6.117840,0,'2016-10-07 03:34:10'),(27,'23','0000000023805','',106.923800,-6.219070,0,'2016-10-07 16:55:41'),(28,'23','0000000023805','',106.923800,-6.219070,0,'2016-10-07 16:56:48'),(29,'23','0000000023805','',106.923800,-6.219070,0,'2016-10-07 16:57:33'),(30,'23','0000000023805','',106.923800,-6.219070,0,'2016-10-07 16:59:04'),(31,'23','0000000023805','',106.923800,-6.219070,0,'2016-10-07 16:59:38'),(32,'23','0000000023805','',106.923800,-6.219070,0,'2016-10-07 17:00:13'),(33,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:52:54'),(34,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:53:18'),(35,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:54:16'),(36,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:55:03'),(37,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:56:16'),(38,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:57:40'),(39,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 15:58:58'),(40,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 16:00:01'),(41,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 16:01:03'),(42,'23','0000000023805','',106.909480,-6.247860,0,'2016-10-09 16:01:59'),(43,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:34:03'),(44,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:35:16'),(45,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:35:31'),(46,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:36:30'),(47,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:37:38'),(48,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:38:36'),(49,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:39:25'),(50,'23','0000000023805','',106.918430,-6.227220,0,'2016-10-10 23:40:26'),(51,'23','0000000023805','',106.919130,-6.227500,0,'2016-10-10 23:41:25'),(52,'23','0000000023805','',106.919130,-6.227500,0,'2016-10-10 23:42:26'),(53,'23','0000000023805','',106.919130,-6.227500,0,'2016-10-10 23:43:27'),(54,'23','0000000023805','',106.919130,-6.227500,0,'2016-10-10 23:44:29'),(55,'23','0000000023805','',106.919130,-6.227500,0,'2016-10-10 23:45:28'),(56,'23','0000000023805','',106.918540,-6.227260,0,'2016-10-10 23:46:28'),(57,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:47:29'),(58,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:48:18'),(59,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:49:29'),(60,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:50:22'),(61,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:51:31'),(62,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:52:23'),(63,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:53:27'),(64,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:54:27'),(65,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-10 23:55:29'),(66,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:00:34'),(67,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:05:34'),(68,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:10:27'),(69,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:15:26'),(70,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:20:24'),(71,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:21:29'),(72,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:22:29'),(73,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:23:29'),(74,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:24:29'),(75,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:25:37'),(76,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:26:37'),(77,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:27:35'),(78,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:28:35'),(79,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:29:35'),(80,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:30:37'),(81,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:31:51'),(82,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:32:52'),(83,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:33:52'),(84,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:34:51'),(85,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:35:52'),(86,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:36:51'),(87,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:37:52'),(88,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:38:52'),(89,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:39:52'),(90,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:41:07'),(91,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:42:08'),(92,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:43:05'),(93,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:44:07'),(94,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:45:08'),(95,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:46:07'),(96,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:47:07'),(97,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:48:07'),(98,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:49:07'),(99,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:50:07'),(100,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:51:07'),(101,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:52:07'),(102,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:53:07'),(103,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:54:09'),(104,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:55:08'),(105,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:56:09'),(106,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:57:08'),(107,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:58:09'),(108,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 00:59:10'),(109,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:00:06'),(110,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:01:11'),(111,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:02:12'),(112,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:03:43'),(113,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:04:43'),(114,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:06:07'),(115,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:07:12'),(116,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:08:10'),(117,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:09:10'),(118,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:10:08'),(119,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:11:05'),(120,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:12:12'),(121,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:13:11'),(122,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:14:10'),(123,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:15:08'),(124,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:16:14'),(125,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:17:10'),(126,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:17:21'),(127,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:18:19'),(128,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:19:17'),(129,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:19:52'),(130,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:21:04'),(131,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:21:16'),(132,'23','0000000023805','',106.918543,-6.227264,0,'2016-10-11 01:22:10'),(133,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:47:24'),(134,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:48:24'),(135,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:49:23'),(136,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 03:50:23'),(137,'23','0000000023805','',106.892655,-6.117388,0,'2016-10-13 03:51:11'),(138,'23','0000000023805','',106.892655,-6.117388,0,'2016-10-13 03:53:05'),(139,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 03:53:39'),(140,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:53:53'),(141,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:54:40'),(142,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 03:55:36'),(143,'23','0000000023805','',106.892747,-6.117663,0,'2016-10-13 03:56:34'),(144,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:57:44'),(145,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 03:57:54'),(146,'23','0000000023805','',106.892906,-6.117629,0,'2016-10-13 03:58:46'),(147,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 03:59:45'),(148,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 04:00:45'),(149,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 04:01:48'),(150,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 04:02:48'),(151,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 04:03:47'),(152,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 04:04:13'),(153,'23','0000000023805','',106.892657,-6.117648,0,'2016-10-13 04:05:14'),(154,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:06:10'),(155,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:06:18'),(156,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:07:17'),(157,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:08:18'),(158,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:09:17'),(159,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:09:30'),(160,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:10:20'),(161,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:11:14'),(162,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:12:16'),(163,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:12:48'),(164,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:13:50'),(165,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:14:56'),(166,'23','0000000023805','',106.892655,-6.117427,0,'2016-10-13 04:16:09'),(167,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:17:23'),(168,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:18:09'),(169,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:19:00'),(170,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:20:10'),(171,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:21:13'),(172,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:22:13'),(173,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:23:14'),(174,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:24:37'),(175,'23','0000000023805','',106.892655,-6.117348,0,'2016-10-13 04:29:22'),(176,'23','0000000023805','',106.892747,-6.117584,0,'2016-10-13 04:39:30'),(177,'23','0000000023805','',106.892747,-6.117584,0,'2016-10-13 04:44:29'),(178,'23','0000000023805','',106.909208,-6.255139,0,'2016-11-03 09:49:39'),(179,'23','0000000023805','',106.909208,-6.255139,0,'2016-11-03 09:50:36'),(180,'23','0000000023805','',106.909208,-6.255139,0,'2016-11-03 09:51:38'),(181,'23','0000000023805','',106.909208,-6.255139,0,'2016-11-03 09:52:14');
/*!40000 ALTER TABLE `driver_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_pos_polisi`
--

DROP TABLE IF EXISTS `jenis_pos_polisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_pos_polisi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_pos_polisi`
--

LOCK TABLES `jenis_pos_polisi` WRITE;
/*!40000 ALTER TABLE `jenis_pos_polisi` DISABLE KEYS */;
INSERT INTO `jenis_pos_polisi` VALUES (1,'Polres','2016-09-11 06:31:11','2016-09-11 06:31:11','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `jenis_pos_polisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_panic`
--

DROP TABLE IF EXISTS `log_panic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_panic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driver_id` int(10) unsigned NOT NULL,
  `lat` double(8,2) NOT NULL,
  `lng` double(8,2) NOT NULL,
  `panic_id` int(10) unsigned NOT NULL,
  `msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_panic`
--

LOCK TABLES `log_panic` WRITE;
/*!40000 ALTER TABLE `log_panic` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_panic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_07_30_154608_create_lokasi_truk',1),('2016_07_30_154643_create_trucks',1),('2016_07_30_154655_create_panic_reports',1),('2016_07_30_154706_create_panic_report_logs',1),('2016_07_30_154720_create_status',1),('2016_07_30_154734_create_users',1),('2016_07_30_154746_create_broadcast',1),('2016_07_30_171932_table_relations',1),('2016_08_07_072549_add_pos_polisi_table',1),('2016_08_07_074845_add_jenis_pos_polisi_table',1),('2016_08_07_143511_add_master_panic',1),('2016_08_07_143858_add_log_panic',1),('2016_08_13_130852_create_last_driver_location',1),('2016_08_13_132832_create_fact_driver_location',1),('2016_08_13_152405_create_gcm_users',1),('2016_08_30_231728_add_api_session',1),('2016_08_30_232947_new_schema_driver_location',1),('2016_08_30_233556_change_schema_panic_report',1),('2016_08_30_233823_change_schema_broadcast',1),('2016_08_31_141702_add_role_to_users_table',1),('2016_08_31_145651_change_push_notification',1),('2016_08_31_153645_add_current_driver_location',1),('2016_09_02_031847_add_location_properties',1),('2016_09_02_033905_add_broadcast_range',1),('2016_09_02_165444_add_panic_report_response',1),('2016_09_02_170052_alter_panic_report',1),('2016_09_05_084526_add_password_to_users',2),('2016_10_21_053452_add_image_severity_panic_report',3),('2016_11_15_231924_add_driver_info_panic_report',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panic`
--

DROP TABLE IF EXISTS `panic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panic`
--

LOCK TABLES `panic` WRITE;
/*!40000 ALTER TABLE `panic` DISABLE KEYS */;
INSERT INTO `panic` VALUES (1,'Panic tingkat biasa','masih biasa',1,'2016-09-17 20:26:17','2016-09-17 20:26:21'),(2,'Test','test deskrips',0,'2016-09-17 14:17:00','2016-09-17 14:20:16'),(5,'Anime','Anime',0,'2016-09-17 16:14:07','2016-09-17 16:14:07'),(6,'Actizve','CreatActizve',1,'2016-09-17 16:15:44','2016-09-17 16:15:44'),(7,'mau apa ?','mau apa ?',1,'2016-09-17 16:17:43','2016-09-17 16:17:43');
/*!40000 ALTER TABLE `panic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panic_report`
--

DROP TABLE IF EXISTS `panic_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panic_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `panic_type_id` int(11) NOT NULL,
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `severity_id` int(11) NOT NULL,
  `driver_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driver_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(9,6) NOT NULL,
  `latitude` decimal(9,6) NOT NULL,
  `message` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OPEN','RESPONDED','CLOSED') COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panic_report`
--

LOCK TABLES `panic_report` WRITE;
/*!40000 ALTER TABLE `panic_report` DISABLE KEYS */;
INSERT INTO `panic_report` VALUES (1,1,'19','','',0,'','',5.000000,5.000000,'Brekele happened on street yoo!','','OPEN','2016-09-02 18:31:36'),(2,1,'19','0000000019102','',0,'','',5.000000,5.000000,'Brekele happened on street yoo!','','OPEN','2016-09-02 18:32:27'),(3,7,'23','0000000023805','',0,'','',-6.117620,106.892720,'hola','','CLOSED','2016-10-07 03:16:01'),(4,7,'23','0000000023805','',0,'','',-6.227264,106.918543,'hellpppp','','CLOSED','2016-10-11 00:21:39'),(5,7,'23','0000000023805','',0,'','',-6.227264,106.918543,'halooi','','CLOSED','2016-10-11 01:17:35'),(6,7,'23','0000000023805','',0,'','',-6.227264,106.918543,'oiii','','CLOSED','2016-10-11 01:20:38'),(7,7,'23','0000000023805','',0,'','',-6.227264,106.918543,'pesen bakso','','CLOSED','2016-10-11 01:21:37'),(8,7,'23','0000000023805','',0,'','',-6.227264,106.918543,'es campur','','OPEN','2016-10-11 01:22:16'),(9,7,'23','0000000023805','',0,'','',-6.117648,106.892657,'ada begal','','CLOSED','2016-10-13 04:02:45'),(10,7,'23','0000000023805','',0,'','',-6.117648,106.892657,'test','','CLOSED','2016-10-13 04:04:28'),(11,7,'23','0000000023805','',0,'','',-6.117427,106.892655,'test','','OPEN','2016-10-13 04:06:28'),(12,7,'23','0000000023805','',0,'','',-6.117427,106.892655,'panic','','CLOSED','2016-10-13 04:08:34'),(13,7,'23','0000000023805','',0,'','',-6.117427,106.892655,'test aja nih','','CLOSED','2016-10-13 04:10:46'),(14,7,'23','0000000023805','',0,'','',-6.117427,106.892655,'halooooooo','','CLOSED','2016-10-13 04:11:36'),(15,7,'23','0000000023805','',0,'','',-6.117348,106.892655,'darurat','','CLOSED','2016-10-13 04:17:00');
/*!40000 ALTER TABLE `panic_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panic_report_response`
--

DROP TABLE IF EXISTS `panic_report_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panic_report_response` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `panic_report_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `response` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panic_report_response`
--

LOCK TABLES `panic_report_response` WRITE;
/*!40000 ALTER TABLE `panic_report_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `panic_report_response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_polisi`
--

DROP TABLE IF EXISTS `pos_polisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_polisi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` double(9,6) NOT NULL,
  `long` double(9,6) NOT NULL,
  `jenis_pos_polisi_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `alamat` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_polisi`
--

LOCK TABLES `pos_polisi` WRITE;
/*!40000 ALTER TABLE `pos_polisi` DISABLE KEYS */;
INSERT INTO `pos_polisi` VALUES (1,'Polres Jakut',-6.129870,106.899870,1,0,'Kebon Bawang','Let kol Sugiono','2016-09-11 06:32:44','2016-10-04 02:24:23','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `pos_polisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `severity`
--

DROP TABLE IF EXISTS `severity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `severity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `severity` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `severity`
--

LOCK TABLES `severity` WRITE;
/*!40000 ALTER TABLE `severity` DISABLE KEYS */;
/*!40000 ALTER TABLE `severity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_device`
--

DROP TABLE IF EXISTS `user_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_device` (
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_device`
--

LOCK TABLES `user_device` WRITE;
/*!40000 ALTER TABLE `user_device` DISABLE KEYS */;
INSERT INTO `user_device` VALUES ('23','APA91bGR-irAFpo5SSVNbi506izqQskadIQPC8eiC5ooxeyCKFglHfytjr-zIToixhimwDJ6gdPktY5hwlU61PEX7ajS4Xf6xPo90lMED42KQBUIyCEj-sk','2016-10-07 02:45:10','2016-11-03 09:49:27');
/*!40000 ALTER TABLE `user_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'admin','email@email.com','082124217369','admin','$2y$10$jJG9HQT5OaTVg9vXI4IX7.IBBYkLUwABrUkOd6D7Q/jDLEN1zO94q','4I9ZSpPcF77e8GJzhAWxRqBhr1gedUfwEc2RklRX8dV7Doz8QL8o9kfpSHM6'),(7,'user1','user@email.com','0192837','staff','$2y$10$8M1XemSxq81syETV/p50SeK/7fiFAlTfupqhmh8ejnr14LhSHrnHa','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-19 19:37:26
