var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();
var request = require('request');

redis.subscribe('test-channel', function(err, count) {
    console.log(count);
});

redis.on('message', function(channel, message) {
    console.log('Message Received: ' + message);
    //console.log('Message Received: ' + message);
    message = JSON.parse(message);
    io.emit(channel + ':' + message.event, message.data);

    /*var formData = {};
    request.post({url:'http://homestead.app/api/v1/update_location', formData: formData}, function optionalCallback(err, httpResponse, body) {
        if (err) {
            return console.error('upload failed:', err);
        }
        console.log('Upload successful!  Server responded with:', body);
    });*/
});

http.listen(3000, function(){
    console.log('Listening on Port 3000');
});