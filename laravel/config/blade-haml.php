<?php return array(


	/**
	 * These options are passed directly to MtHaml's environment constructor
	 */
	'mthaml' => [
		'environment' => 'php',
		'options' => [
			'enable_escaper' => false,
		],
		'filters' => []
	]

);
