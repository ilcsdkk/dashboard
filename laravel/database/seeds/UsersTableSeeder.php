<?php

use Illuminate\Database\Seeder;
use Ntmc\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "seed user admin";
        User::create([
            'username' => 'admin',
            'email' => 'email@email.com',
            'phone_number' => "082124217369",
            'role' => "admin",
            'password' => '$2y$10$jJG9HQT5OaTVg9vXI4IX7.IBBYkLUwABrUkOd6D7Q/jDLEN1zO94q',
            'remember_token' => ""
        ]);
    }
}
