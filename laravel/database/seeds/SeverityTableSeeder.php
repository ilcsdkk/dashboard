<?php

use Illuminate\Database\Seeder;
use Ntmc\Models\Severity;


class SeverityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //check not production
        if (App::environment() === 'production') {
            exit('I just stopped you before getting fired.');
        }
        echo "create severity seeder";
        Severity::create([
            'severity' => 'Darurat',
            'created' => date('Y-m-d H:i:s')
        ]);

        Severity::create([
            'severity' => 'Bantuan',
            'created' => date('Y-m-d H:i:s')
        ]);

        Severity::create([
            'severity' => 'Informasi',
            'created' => date('Y-m-d H:i:s')
        ]);      

    }
}
