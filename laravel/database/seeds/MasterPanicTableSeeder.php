<?php

use Illuminate\Database\Seeder;
use Ntmc\Models\Panic;


class MasterPanicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //check not production
        if (App::environment() === 'production') {
            exit('I just stopped you before getting fired.');
        }
        echo "create master panic seeder";
        Panic::create([
            'name' => 'Kecelakaan',
            'deskripsi' => 'Deskrips Kecelakaan',
            'active' => 1,
            'created' => date('Y-m-d H:i:s'),
            'updated' => date('Y-m-d H:i:s')
        ]);

        Panic::create([
            'name' => 'Dibegal',
            'deskripsi' => 'Deskrips Dibegal',
            'active' => 1,
            'created' => date('Y-m-d H:i:s'),
            'updated' => date('Y-m-d H:i:s')
        ]);

    }
}
