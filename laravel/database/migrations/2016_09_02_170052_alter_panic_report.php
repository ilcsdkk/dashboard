<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPanicReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('panic_report', function($table)
        {
            $table->string('card_id')->after('driver_id');
            $table->string('truck_id')->after('card_id');
            $table->enum('status', ['OPEN', 'RESPONDED', 'CLOSED'])->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
