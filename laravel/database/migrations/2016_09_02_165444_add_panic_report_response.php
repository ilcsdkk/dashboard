<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPanicReportResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panic_report_response', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('panic_report_id');
            $table->integer('users_id');
            $table->string('response', 400);
            $table->dateTime('created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('panic_report_response');
    }
}
