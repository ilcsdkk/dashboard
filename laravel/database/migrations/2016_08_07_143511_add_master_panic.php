<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasterPanic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('deskripsi');
            $table->tinyInteger('active');
            $table->dateTime('created');
            $table->dateTime('updated');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('panic');
    }
}
