<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('current_driver_location', function($table)
        {
            $table->string('card_id')->after('driver_id');
            $table->string('truck_id')->after('card_id');
        });

        Schema::table('driver_location', function($table)
        {
            $table->string('card_id')->after('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
