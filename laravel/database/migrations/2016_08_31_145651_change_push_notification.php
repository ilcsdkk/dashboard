<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePushNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('gcm_users');
        Schema::create('user_device', function (Blueprint $table) {
            $table->char('driver_id', 13);
            $table->string('device_id', 255);
            $table->dateTime('created');
            $table->dateTime('updated');

            $table->primary('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_device');
    }
}
