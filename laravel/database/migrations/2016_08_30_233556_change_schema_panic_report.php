<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSchemaPanicReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::disableForeignKeyConstraints();
        
        Schema::drop('panic_reports');
        Schema::drop('panic_report_logs');

        Schema::create('panic_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('panic_type_id');
            $table->char('driver_id', 13);
            $table->double('longitude');
            $table->double('latitude');
            $table->string('message', 400);
            $table->dateTime('created');
        });
        
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('panic_report');
    }
}
