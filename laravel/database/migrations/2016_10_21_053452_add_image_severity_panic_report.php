<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageSeverityPanicReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('severity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('severity', 45);
            $table->dateTime('created');
            $table->dateTime('updated');
            $table->dateTime('deleted');
        });

        Schema::table('panic_report', function($table)
        {
            $table->integer('severity_id')->after('truck_id');
            $table->string('image')->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
