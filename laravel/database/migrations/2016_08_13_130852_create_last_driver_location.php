<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastDriverLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_driver_location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_id');
            $table->integer('driver_id')->unsigned();
            $table->float('lat');
            $table->float('lng');
            $table->tinyInteger('active');
            $table->dateTime('created');
            $table->dateTime('updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('last_driver_location');
    }
}
