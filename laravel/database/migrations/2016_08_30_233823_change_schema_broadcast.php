<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSchemaBroadcast extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::disableForeignKeyConstraints();
        
        Schema::drop('broadcast');

        Schema::create('broadcast_message', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id');
            $table->integer('broadcast_type_id');
            $table->string('title', 160);
            $table->string('description', 400);
            $table->dateTime('created');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('broadcast_message');
    }
}
