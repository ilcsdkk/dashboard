<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewSchemaDriverLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::drop('lokasi_truk');
        Schema::drop('last_driver_location');
        Schema::drop('fact_driver_location');

        Schema::create('driver_location', function (Blueprint $table) {
            $table->increments('id');
            $table->char('driver_id', 13);
            $table->char('truck_id', 13);
            $table->double('longitude');
            $table->double('latitude');
            $table->double('heading');
            $table->dateTime('created');
        });


        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('driver_location');
    }
}
