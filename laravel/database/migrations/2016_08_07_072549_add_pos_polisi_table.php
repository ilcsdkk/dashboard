<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPosPolisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_polisi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->float('lat');
            $table->float('long');
            $table->integer('jenis_pos_polisi_id');
            $table->integer('parent');
            $table->longText('alamat');
            $table->longText('contact_person');
            //$table->dateTime('date');
            //$table->integer('Trucks_Id')->unsigned();
            $table->dateTime('created');
            $table->dateTime('updated');
            $table->dateTime('deleted');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pos_polisi');
    }
}
