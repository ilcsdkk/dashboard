<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogPanic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_panic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('card_id');
            $table->integer('driver_id')->unsigned();
            $table->float('lat');
            $table->float('lng');
            $table->integer('panic_id')->unsigned();
            $table->string('msg');
            $table->integer('level')->unsigned();
            $table->tinyInteger('active');
            $table->dateTime('created');
            $table->dateTime('updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('log_panic');
    }
}
