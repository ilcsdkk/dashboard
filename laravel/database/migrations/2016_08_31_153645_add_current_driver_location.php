<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentDriverLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('current_driver_location', function (Blueprint $table) {
            $table->char('driver_id', 13);
            $table->float('longitude');
            $table->float('latitude');
            $table->float('heading');
            $table->dateTime('updated');

            $table->primary('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('current_driver_location');
    }
}
