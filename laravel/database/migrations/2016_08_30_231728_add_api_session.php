<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_session', function (Blueprint $table) {
            $table->char('token', 13);
            $table->char('driver_id', 13);
            $table->char('card_id', 13);
            $table->char('truck_id', 13);
            $table->char('pin', 4);
            $table->boolean('authorized');
            $table->dateTime('expired');
            $table->dateTime('created');
            $table->primary('token');
            $table->index('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_session');
    }
}
