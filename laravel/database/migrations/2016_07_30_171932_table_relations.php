<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('lokasi_truk', function (Blueprint $table) {
            $table->foreign('Trucks_Id')->references('id')->on('Trucks');
        });

         Schema::table('panic_reports', function (Blueprint $table) {
            $table->foreign('lokasi_truk_id')->references('id')
                ->on('lokasi_truk');
            $table->foreign('status_id1')->references('id')->on('status');
        });

          Schema::table('panic_report_logs', function (Blueprint $table) {
            $table->foreign('panic_reports_id')->references('id')
                ->on('panic_reports');
            $table->foreign('panic_reports_lokasi_truk_id')->references('id')
                ->on('lokasi_truk');
            $table->foreign('status_id1')->references('id')
                ->on('status');
            $table->foreign('users_id')->references('id')
                ->on('users');
        });

          Schema::table('broadcast', function (Blueprint $table) {
            $table->foreign('users_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
