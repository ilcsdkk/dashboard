<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueKeyFromParameterLang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameter_lang', function (Blueprint $table) {
            //
            //$table->dropPrimary('parameter_lang_parameter_id_primary');
            //$table->dropPrimary('parameter_lang_parameter_lang_id_primary');

            //$table->dropPrimary();
            $table->dropPrimary(['parameter_id', 'parameter_lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameter_lang', function (Blueprint $table) {
            $table->primary(['parameter_id', 'parameter_lang_id']);
        });
    }
}
