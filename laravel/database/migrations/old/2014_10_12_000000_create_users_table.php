<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            //$table->increments('id');
            //$table->string('user_first_name');
            //$table->string('user_email')->unique();
            if (Schema::hasColumn('user_id', 'user_first_name', 'user_password'))
            {
                //
                $table->renameColumn('user_id','id');
                $table->renameColumn('user_first_name', 'first_name');
                $table->renameColumn('user_last_name', 'last_name');
                $table->renameColumn('user_name', 'username');
                $table->renameColumn('user_email', 'email');
                $table->renameColumn('user_password', 'password');
                $table->string('role_id');
                $table->string('confirmation_code');
                $table->rememberToken();
                $table->renameColumn('user_image', 'image');
                $table->renameColumn('user_fb_id', 'fb_id');
                $table->renameColumn('user_fb_token', 'fb_token');
                $table->renameColumn('user_gplus_id', 'gplus_id');
                $table->renameColumn('user_gplus_token', 'gplus_token');
                $table->renameColumn('user_twitter_id', 'twitter_id');
                $table->renameColumn('user_twitter_token', 'twitter_token');
                $table->renameColumn('user_pwd_key', 'password_key');
                $table->renameColumn('user_pwd_key_valid_to', 'pwd_key_valid_to');
                $table->renameColumn('user_country_id', 'country_id');
                $table->renameColumn('user_province_id', 'province_id');
                $table->renameColumn('user_city_id', 'city_id');
                $table->renameColumn('user_district_id', 'district_id');
                $table->renameColumn('user_address', 'address');
                $table->renameColumn('user_post_code', 'post_code');
                $table->renameColumn('user_phone_no', 'phone_no');
                $table->renameColumn('user_mobile_no', 'mobile_no');
                $table->renameColumn('user_birthday', 'birthday');
                $table->renameColumn('user_nationality_country_id', 'nationality_country_id');
                $table->renameColumn('user_passport_no', 'passport_no');
                $table->renameColumn('user_passport_expiry_date', 'passport_expiry_date');
                $table->renameColumn('user_identity_type', 'identity_type');
                $table->renameColumn('user_identity_no', 'identity_no');
                $table->renameColumn('user_last_login_time', 'last_login_time');
                $table->renameColumn('user_status', 'status');
                $table->renameColumn('user_created_at', 'created_at');
                $table->renameColumn('user_created_by', 'created_by');
                $table->renameColumn('user_updated_at', 'updated_at');
                $table->renameColumn('user_updated_by', 'updated_by');
                $table->renameColumn('user_deleted_at', 'deleted_at');
                $table->renameColumn('user_deleted_by', 'deleted_by');
                //$table->timestamps();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
