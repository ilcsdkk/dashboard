<?php namespace Ntmc\Providers;

use Illuminate\Foundation\AliasLoader as CoreAliasLoader;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider as CoreServiceProvider;
use Blade;


/**
* @package		Loader Service
* @author 		kb.hartanto@gmail.com
**/
class LoaderServiceProvider extends CoreServiceProvider
{
  /*
  *	Define variable
  */
  protected $providers = [
        ModulesServiceProvider::class,
        \Gbrock\Table\Providers\TableServiceProvider::class,
  ];

  protected $alias = [
        //'Monitoring' => "Ntmc\Facade\MonitoringFacade",
        'Validation' => "Ntmc\Facade\ValidationFacade",
    'Table' => "Ntmc\Facade\TableFacade",
    'FormTable'      => 'Gbrock\Table\Facades\Table',
  ];

  public function boot()
  {
        Blade::directive('openphp', function($expression) {
            return "<?php";
        });
        Blade::directive('closephp', function($expression) {
            return "?>";
        });
  }
  public function register()
  {
    // register provider
    foreach ($this->providers as $provider) {
          $this->app->register($provider);
      }

        // register alias library
        $loader = CoreAliasLoader::getInstance();
        foreach ($this->alias as $k => $v) {
          $loader->alias($k, $v);
        }
  }
}

// EOF Gamespark
