<?php

namespace Ntmc\Providers;

use Illuminate\Routing\Router;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Ntmc\Controllers';
    protected $modules = 'Ntmc';

  public function boot()
    {
        // set default path for Ntmc apps
        $path = app_path('../Ntmc/');
        $views  = $path.'/Views';
        $trans  = $path.'/Translations';
        //load Ntmc module
        $this->mapNtmcRoutes();
        if($this->files->isDirectory($views)) $this->loadViewsFrom($views, $this->modules);
        if($this->files->isDirectory($trans)) $this->loadTranslationsFrom($trans,$this->modules);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
         $this->files = new Filesystem;
         \View::addLocation(realpath(base_path() . '/../themes/'));

    }

    /**
     * Define the "Ntmc" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapNtmcRoutes()
    {
        \Route::group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function () {
            require app_path('../Ntmc/routes.php');
        });
    }

}
