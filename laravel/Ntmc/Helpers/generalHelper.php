<?php

/**
* @package General halpers
* @since 13-Jun-2016
* @author kb.harntamto@gmail.com
**/


if (!function_exists('show_404'))
{
	function show_404()
	{
		return \App::abort(404);
	}
}

if (!function_exists('getFormType'))
{
	function getFormType($table='',$column='')
	{
		//modification option
		switch ($table) {
			case 'slideshow_img':
				$table = 'slideshow_image';
				break;
			case 'slideshow_img_lang':
				$table = 'slideshow_image_lang';
				break;
			default:
				break;
		}

		// check type
		$length = \DB::connection()->getDoctrineColumn('tbl_'.$table, $column)->getLength();
		$type = \DB::connection()->getDoctrineColumn('tbl_'.$table, $column)->getType();

		switch ($type) {
			case 'String':
				if ( $length <= 251 ) {
					$return = 'text';
				} else {
					$return = 'textarea';
				}
				break;
			case 'Text':
				$return = 'textarea';
				break;
			case 'Boolean':
			case 'Integer':
				$return = 'number';
				break;
			default:
				exit($type);
				break;
		}

		return $return;
	}
}


if (!function_exists('str_ucfirst'))
{
	function str_ucfrist($str)
	{
		return ucfirst(str_replace(['_','-'], ' ', $str));
	}
}
// EOF