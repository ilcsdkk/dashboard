<?php namespace Ntmc\Library;

use Illuminate\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FilesController {

  protected $filesystem;
  protected $directory;
  protected $extension;
  protected $size;
  protected $files;
  protected $image;
  protected $content;

  function __construct(Filesystem $filesystem,FilesModel $files, ContentModel $content)
  {
    $this->filesystem = $filesystem;
    $this->files = $files;
    $this->content = $content;
  }
  protected function getUrl($path)
  {
      return  url($path);
  }
  protected function makeFileName($filename)
  {
      return Sha1($filename .time() . time()) ;
  }
  protected function makeThumbnails($path,$filename,$ext='png')
  {
    $image = Image::make($path); $thumb = ['small' => 100,'medium' => 200];

    foreach ($thumb as $key => $width) {
      $save = public_path($path.DIRECTORY_SEPARATOR.$filename."_".$key.".".$ext);
      $image->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
      });
      $image->save($save);
    }
  }
  protected function getFolder($file)
  {
    // check image or not
    try {
      $makeImage = \Image::make($file);
      $folder = 'images';
    } catch (Exception $e) {
      $folder = 'files';
    }

    return $folder;
  }
  protected function getFile($path)
  {
      return $this->filesytem->get($path);
  }
  protected function getFileSize($path)
  {
      return $this->filesytem->size($path);
  }
  protected function getDataUrl($mime, $path)
  {
    // create data url with base64 encode for private storage
      $base = base64_encode($this->getFile($path));
      return 'data:' .  $mime . ';base64,'.  $base;
  }
  protected function getJsonBody($filename, $mime, $path)
  {
      return [
          'files' => [
              'filename'  => $filename,
              'mime'      => $mime,
              'size'      => $this->getFileSize($path),
              'dataURL'   => $this->getDataUrl($mime, $path)
          ]
      ];
  }
  public function upload($file = false , $allow_ext = false, $allow_size = false)
  {
    $filename='';
    $folder = $this->get_folder($file);
    //check valid files before upload
    $this->extension = $file->getClientOriginalExtension();
    if ($allow_ext)
    {
      $allow_exts = explode(',',str_replace(" ", "",$allow_ext));
      if ( !in_array($this->extension, $allow_exts) ) return ['error' => true, 'message' => 'Input your must extension '.$allow_ext, 'redirect' => URL::previous()];
    }
    //check allow size
    if ($allow_size) {}
    // create new files name
    $filename = $this->makeFileName($file->getClientOriginalName());
    $ext = ".".strtolower($this->extension);
    //upload proses
    $upload = $file->move(public_path('content/'.$folder),$filename.$ext);

    if ($upload)
    {
      // set for save to db
      $data['type'] = $folder ;
      $data['folder'] = 'content/'.$folder ;
      $data['mime'] = $file->getClientMimeType();
      $data['original_filename'] = $file->getClientOriginalName();
      $data['filename'] = $filename.$ext;
      $data['size'] = $file->getClientSize();

      $this->files->insert($data);
      $save = $this->files->where($data)->orderBy('_id','desc')->get()->first();
      $location = $folder . '/' . $save->_id;
      if (!isAjax()) return $location;
      return stripslashes(json_encode(['filelink' => $this->getUrl($location),'filename' => $file->getClientOriginalName()]));
    } else {
      error_log("Error moving file: ".$file->getClientOriginalName());
    }
  }
  public function handler($file = false , $allow_ext = false, $allow_size = false , $convert = false)
  {
    $result = '';
    if (!$file) $file = Input::file('file');
    if (empty($file)) return;

    // check allow extension
    if (is_array($file)):
      foreach ($file as $key => $files) {
        $result[] = $this->upload($files , $allow_ext, $allow_size, $convert);
      }
    else:
      $result = $this->upload($file, $allow_ext, $allow_size, $convert);
    endif;

        return $result;

  }
  public function wysiwyg_images()
    {
        $image = $this->files->where(['folder' => 'images'])->get();

        foreach ($image as $key => $value) {
          $images[$key]['thumb'] = url('images/'.$value->id."/200");
          $images[$key]['image'] = url('images/'.$value->id);
        }

        return json_encode($images);
    }
    public function wysiwyg_files()
    {

    $image = $this->files->where(['type' => 'files'])->get();

        foreach ($image as $key => $value) {
            $size = [' B', ' KB', ' MB', ' GB'];
            $factor = floor((strlen($bytes = $value->size) - 1) / 3);
          $files[$key]['title'] = $value->original_filename;
          $files[$key]['name'] = $value->original_filename;
          $files[$key]['link'] = url('content/files/'.$value->filename);
          $files[$key]['size'] = (float)sprintf("%.2f", $bytes / pow(1024, $factor)) . @$size[(int)$factor];
        }
        return json_encode($files);
    }
    public function showImage($id='',$width=null,$height=null)
    {
      $files = $this->files->find($id);
      //dd ($files);
      $images = public_path($files->folder.'/'.$files->filename);
      $img = \Image::make($images);

      if ( $width != null && $height != null )
      {
        $img->resize($width,$height);
      } elseif ( $width != null || $height != null ) {
        $img->resize($width, $height, function ($constraint) {
          $constraint->aspectRatio();
      });
      }

      return $img->response('jpg');
    }
    public function showFiles($id='')
    {
      # code...
    }
}
