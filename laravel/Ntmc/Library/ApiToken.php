<?php  namespace Ntmc\Library;

/**
**	@package API Token Validation and management
**  @author djati.satria@gmail.com
**/


use Ntmc\Models\ApiSession;
use Exception;

class ApiToken {
    protected static $sessExpirationSec = 3600;

    public function __construct(){

    }

    public static function countNewExpiredTime(){
        return date('Y-m-d H:i:s', time() + self::$sessExpirationSec);
    }

    public static function get($token){
        $session = ApiSession::find($token);
        
        /*if(!$session || strtotime($session->expired) < time()){
            throw new Exception('Session not found or expired', 301);
        }*/

        /*if($session->authorized == false){
            throw new Exception('Session not authorized', 302);
        }*/

        // Update expired time when session accessed
        if($session){
            $session->expired = self::countNewExpiredTime();
            $session->save();
        }

        return $session;
    }

    public static function authorize($token, $pin){
        $session = ApiSession::find($token);

        if(!$session || strtotime($session->expired) < time()){
            throw new Exception('Session not found or expired', 301);
        }

        // Validate PIN
        /*if($session->pin != $pin){
            $session->delete();

            throw new Exception('Incorrect PIN, this session removed please login again', 502);
        }*/

        // Set Authorized so this token can be used
        $session->authorized = true;
        $session->save();

        return $session;
    }

    public static function destroy($token){
        $session = ApiSession::find($token);

        // Dont care whenever session is exists or not
        if($session){
            $session->delete();
        }

        return true;
    }

    public static function create($driver_id, $card_id, $pin){
        $token = uniqid();

        $session = new ApiSession();
        $session->token = $token;
        $session->driver_id = $driver_id;
        $session->card_id = $card_id;
        $session->pin = $pin;
        $session->authorized = false;
        $session->expired = self::countNewExpiredTime();
        $session->created = date('Y-m-d H:i:s');

        $session->save();

        return $token;
    }

    
}


/* End of file Table.php */
/* Location: ./system/libraries/Table.php */
