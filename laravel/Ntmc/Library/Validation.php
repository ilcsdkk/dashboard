<?php namespace Ntmc\Library

use Illuminate\Validation\Factory as Validator;

/**
*	@package Library Laravel
* 	@author  kb.hartanto@gmail.com
**/

class Validation {

  /**
   * Validator
   *
   * @var \Illuminate\Validation\Factory
   */
  protected $validator;

  /**
   * Validation data key => value array
   *
   * @var Array
   */
  protected $data = array();

  /**
   * Validation errors
   *
   * @var Array
   */
  protected $errors = array();

  /**
   * Validation rules
   *
   * @var Array
   */
  protected $rules = array();

  /**
   * Custom Validation Messages
   *
   * @var Array
   */
  protected $messages = array();

  public function __construct(Validator $validator)
  {
    $this->validator = $validator;
  }
  /*
  *	Set rules for validate
  */
  public function rules($rules='')
  {
    $this->rules = $rules;
    return $this;
  }

  /**
   * Set data to validate
   *
   * @return \Gamespark\Resources\libraries\validation
   */
  public function with(array $data)
  {
    $this->data = $data;

    return $this;
  }

  /**
   * Validation passes or fails
   *
   * @return boolean
   */
  public function passed($data='', $rules='', $messages='')
  {
    // variable if we need manual validation
    $data = $data ? $data : $this->data;
    $rules = $rules ? $rules : $this->rules;
    $messages = $messages ? $messages : $this->messages;

     $validator = $this->validator->make($data, $rules, $messages);

    if ($validator->fails() )
    {
      $this->errors = $validator->messages();
      return false;
    }


    return true;
  }

  /**
   * Return errors, if any
   *
   * @return array
   */
  public function errors()
  {
    return $this->errors;
  }

}
