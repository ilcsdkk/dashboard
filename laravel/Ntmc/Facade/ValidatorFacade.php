<?php namespace Ntmc\Facade;

class ValidatorFacade extends \Illuminate\Support\Facades\Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Ntmc\Library\Validator'; }

}
