<?php

/**
*   @package    Auth Controller for Mobile Device API
*   @author     kb.hartanto@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;


use Validator;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Crypt;
use Flash;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\SiabKartu;
use Ntmc\Models\SiabSupir;
use Ntmc\Models\CurrentDriverLocation;
use Ntmc\Library\ApiToken;
use Exception;
use StdClass;

class DeviceAuthController extends BaseController
{
    public function __construct()
    {
        //DISABLED: $this->middleware($this->guestMiddleware(), ['except' => ['logout','getLogout']]);
    }

    // @method: POST
    // Login 
    public function login(Request $request){
        try{
            // Get Raw content
            $request_data = json_decode($request->getContent());
            $CardId = $request_data->CardId;

            // Get Driver Data from SIAB
            $cardDetail = SiabKartu::find($CardId);
            
            if($cardDetail->NomorHandphone == ''){
                throw new Exception('Need Update Cellphone Number', 501);
            }
            
            // Generate one time password (PIN)
            $pin = rand(1000, 9999);

            // Create session and token
            $token = ApiToken::create($cardDetail->SupirId, $CardId, $pin);

            // Send Notification
            SiabSupir::save($cardDetail->SupirId, $cardDetail->NomorHandphone, $pin);
            
            // Create Response
            $response = new StdClass();
            $response->Status = 'Success';
            $response->DriverId = $cardDetail->SupirId;
            $response->TokenKey = $token;

            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

    // @method: POST
    // After receiving Token from SMS, API need to call again to authorize the token match 
    public function authorizeToken(Request $request){
        try{
            // Get Raw content
            $request_data = json_decode($request->getContent());
            $PIN = $request_data->PIN;

            // Authorize Token
            $Token = $request->header('Token');
            $session = ApiToken::authorize($Token, $PIN);

            // Get Card Detail from SIAB
            $cardDetail = SiabKartu::find($session->card_id);
            
            $response = new StdClass();
            $response->Status = 'Success';
            $response->DriverName = $cardDetail->NamaSupir;
            $response->CompanyName = $cardDetail->NamaPerusahaan;
            $response->Photo = $cardDetail->PhotoSupir;
            $response->PhoneNumber = $cardDetail->NomorHandphone;
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

    // @method: POST
    // Login 
    public function logout(Request $request){
        $Token = $request->header('Token');

        try{
            $session = ApiToken::get($Token);
            ApiToken::destroy($Token);

            // Delete last location date_add
            $location = CurrentDriverLocation::find($session->driver_id);
            if($location){
                $location->delete();
            }
        }catch(Exception $e){
            // Just continue if exception
        }
        
        $response = new \StdClass();
        $response->Status = 'Success';

        echo (new ApiResponseBase())->fillResponse($response);
    }
}
