<?php

namespace Ntmc\Controllers;

use Illuminate\Http\Request;
use Ntmc\Models\JenisPosPolisi;
use DB;

class JenisPosPolisiController extends BaseController
{
  public function index(Request $request)
  {
    $querystring = $this->_buildQueryString($request);

    $jenisPosPolisi = JenisPosPolisi::Pagination();

    return View('jenis-pos-polisi.index', compact('jenisPosPolisi', 'querystring'));
  }

  public function create()
  {
    $jenisPosPolisi = JenisPosPolisi::Pagination();
    return view('jenis-pos-polisi.create', compact('jenisPosPolisi'));
  }

  public function store(Request $request)
  {
    $this->validate($request, [
        'nama' => 'required|unique:jenis_pos_polisi,nama',
    ], [
        'required' => 'Kolom :attribute diperlukan!',
        'unique' => ':attribute sudah ada yang punya.'
    ]);

    $jenisPosPolisi = JenisPosPolisi::create($request->all());

    \Flash::success('Jenis Pos Polisi tersimpan.');
    return redirect()->route('jenis-pos-polisi.edit', [$jenisPosPolisi->id]);
  }

  public function show($id)
  {
    $item = JenisPosPolisi::findOrFail($id);

    return view('jenis-pos-polisi.show', compact('item'));
  }

  public function edit($id)
  {
    $jenisPosPolisi = JenisPosPolisi::Pagination();
    $item = JenisPosPolisi::findOrFail($id);

    return view('jenis-pos-polisi.edit', compact('jenisPosPolisi', 'item'));
  }

  public function update(Request $request, $id)
  {
    $jenisPosPolisi = JenisPosPolisi::findOrFail($id);

    $this->validate($request, [
        'nama' => 'required|unique:jenis_pos_polisi,nama,' . $jenisPosPolisi->id,
    ], [
        'required' => 'Kolom :attribute diperlukan!',
        'unique' => ':attribute sudah ada yang punya.'
    ]);

    $jenisPosPolisi->update($request->all());

    \Flash::success('Jenis Pos Polisi diperbaharui.');
    return redirect()->back();
  }

  public function destroy($id)
  {
    JenisPosPolisi::findOrFail($id)->delete();

    \Flash::success('Jenis Pos Polisi berhasil dihapus.');
    return redirect()->back();
  }



  //PRIVATE methods
  private function _buildQueryString($request)
  {
    $querystring = null;

    if( $request->has('type') ) {
      $querystring['type'] = $request->has('type') ? $request->type : '';
    } elseif($request->has('q')) {
      $querystring['q'] = $request->has('q') ? $request->q : '';
    }

    return $querystring;
  }
  //END PRIVATE methods
}
