<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Ntmc\Models\User;
use Validator;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Exception;
use Ntmc\Library\ApiToken;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\UserDevice;
use App\Exceptions\ApiBusinessException;
use PushNotif;

class DeviceController extends BaseController
{
    // @method: POST
    public function postAction(Request $request){
        try{
            // Get Token
            $Token = $request->header('Token');
            $session = ApiToken::get($Token);

            // Get Raw content
            $request_data = json_decode($request->getContent());
            $DeviceId = $request_data->DeviceId;

            // Upsert
            $exists = UserDevice::find($session->driver_id);
            if($exists){
                $exists->device_id = $DeviceId;
                $exists->updated = date('Y-m-d H:i:s');

                $exists->save();
            }else{
                $userDevice = new UserDevice();
                $userDevice->driver_id = $session->driver_id;
                $userDevice->device_id = $DeviceId;
                $userDevice->created = date('Y-m-d H:i:s');

                $userDevice->save();
            }

            $response = new StdClass();
            $response->Status = 'Success';
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

    public function broadcastAlert(Request $request)
    {
        if ( $request->ajax() ) {
            try {
                // Get Token
                $message = $request->get('message');
                $longitude = $request->get('longitude');
                $latitude = $request->get('latitude');

                $driver_ids = $request->get('driver_ids');

                $collection = [];
                foreach ($driver_ids as $driver_id) {
                    // broadcast
                    $exists = UserDevice::find($driver_id);
                    if ($exists) {
                        $deviceToken = $exists->device_id;

                        $message = [];
                        $message['type'] = "Broadcast Message";
                        $message['latitude'] = $latitude; //$request->lat
                        $message['longitude'] = $longitude; //$request->lng
                        $message['description'] = $message;
                        $message['timestamp'] = round(microtime(true) * 1000);
                        $message['to'] = $deviceToken;

                        // Send the notification to the device with a token of $deviceToken
                        $collection = PushNotif::app('appNameAndroid')
                            ->to($deviceToken)
                            ->send($message);
                    }
                }

                $response = new StdClass();
                $response->SentCount = count($driver_ids);
                $response->Message = $collection;

                echo (new ApiResponseBase())->fillResponse($response);
            }catch(Exception $e){
                echo (new ApiResponseBase())->fillException($e);
            }
        }
    }
}
