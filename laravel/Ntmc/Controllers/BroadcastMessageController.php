<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Ntmc\Models\User;
use Validator;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Exception;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\BroadcastMessage;
use Ntmc\Library\ApiToken;
use App\Exceptions\ApiBusinessException;


class BroadcastMessageController extends BaseController
{
    public function __construct()
    {
        
    }

    // @method: POST
    // Post From Dashboard
    public function postAction(Request $request){
        try{

            // Save Location for History
            $broadcastMessage = new BroadcastMessage();
            $broadcastMessage->broadcast_type_id = $request->input('BroadcastTypeId');
            $broadcastMessage->title = $request->input('Title');
            $broadcastMessage->description = $request->input('Description');
            $broadcastMessage->longitude = $request->input('Longitude');
            $broadcastMessage->latitude = $request->input('Latitude');
            $broadcastMessage->broadcast_range_meter = $request->input('BroadcastRangeMeter');
            $broadcastMessage->created = date('Y-m-d H:i:s');
            $broadcastMessage->save();

            $broadcastMessageId = DB::getPdo()->lastInsertId();

            // Find All Device within range
            
            // Push Notif to Devices

            // Send Response
            $response = new StdClass();
            $response->Status = 'Success';
            $response->BroadcastId = $broadcastMessageId;
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }
}
