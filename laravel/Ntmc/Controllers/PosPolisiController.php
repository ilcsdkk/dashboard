<?php

namespace Ntmc\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Ntmc\Models\PosPolisi;

class PosPolisiController extends BaseController
{

  public function index(Request $request)
  {
    $querystring = $this->_buildQueryString($request);
    $posPolisi = PosPolisi::Pagination();

    return View('pos-polisi.index', compact('posPolisi', 'querystring'));
  }

  public function create()
  {
    $posPolisi = PosPolisi::Pagination();

    return view('pos-polisi.create', compact('posPolisi'));
  }

  public function store(Request $request)
  {
    $this->validate($request, [
        'nama' => 'required|unique:pos_polisi,nama',
        'lat' => 'required',
        'long' => 'required',
        'jenis_pos_polisi_id' => 'required',
        'alamat' => 'required',
        'contact_person' => 'required',
    ], [
        'required' => 'Kolom :attribute diperlukan!',
        'unique' => ':attribute sudah ada yang punya.'
    ]);

    $data = $request->all();
    $posPolisi = PosPolisi::create($data);

    \Flash::success('Pos Polisi tersimpan.');
    return redirect()->route('pos-polisi.edit', [$posPolisi->id]);
  }
  public function show($id)
  {
    $pos = PosPolisi::findOrFail($id);

    return view('pos-polisi.show', compact('pos'));
  }

  public function edit($id)
  {
    $posPolisi = PosPolisi::Pagination();
    $pos = PosPolisi::findOrFail($id);

    return view('pos-polisi.edit', compact('posPolisi', 'pos'));
  }

  public function update(Request $request, $id)
  {
    $posPolisi = PosPolisi::findOrFail($id);

    $this->validate($request, [
        'nama' => 'required|unique:pos_polisi,nama,' . $posPolisi->id,
        'lat' => 'required',
        'long' => 'required',
        'jenis_pos_polisi_id' => 'required',
        'alamat' => 'required|unique:pos_polisi,alamat,' . $posPolisi->id,
        'contact_person' => 'required'
    ], [
        'required' => 'Kolom :attribute diperlukan!',
        'unique' => ':attribute sudah ada yang punya.'
    ]);

    $posPolisi->update($request->all());

    \Flash::success('Pos Polisi diperbaharui.');
    return redirect()->back();
  }
  public function destroy($id)
  {
      PosPolisi::findOrFail($id)->delete();

      \Flash::success('Pos Polisi berhasil dihapus.');
      return redirect()->back();
  }


  public function getPosition( Request $request )
  {
    if ( $request->ajax() ) {

        try {
            $item = PosPolisi::select('lat', 'long')->findOrFail($request->val);
            return response()->json( $item );
        } catch (ModelNotFoundException $e) {
            $item = ['lat' => -6.174752, 'long' => 106.827201];
            return response()->json( $item );
        }

    }

    exit();
  }

  //PRIVATE methods
  private function _buildQueryString($request)
  {
    $querystring = null;

    if( $request->has('type') ) {
      $querystring['type'] = $request->has('type') ? $request->type : '';
    } elseif($request->has('q')) {
      $querystring['q'] = $request->has('q') ? $request->q : '';
    }

    return $querystring;
  }
  //END PRIVATE methods
}
