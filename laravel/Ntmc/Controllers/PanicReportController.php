<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Ntmc\Models\User;
use Validator;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Exception;
use Ntmc\Library\ApiToken;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\PanicReport;
use Ntmc\Models\SiabKartu;
use App\Exceptions\ApiBusinessException;

class PanicReportController extends BaseController
{
    public function __construct()
    {
        //DISABLED: $this->middleware($this->guestMiddleware(), ['except' => ['logout','getLogout']]);
    }

    // @method: GET
    // Login 
    public function getAction(Request $request)
    {
        try{
            // Get Logon Session

            // Get Location Markers
            $panicReports = DB::table('panic_report')
                            ->select(
                                'panic_report.id AS PanicReportId',
                                'card_id AS CardId',
                                'driver_id AS DriverId', 
                                'truck_id AS TruckId', 
                                'severity_id AS SeverityId',
                                'severity AS Severity',
                                'driver_name AS DriverName', 
                                'driver_phone AS DriverPhone', 
                                'message AS Message', 
                                'longitude AS Longitude', 
                                'latitude AS Latitude', 
                                'panic_report.created AS Created',
								                'image AS Image'
                            )
                            ->join('severity', 'severity.id', '=', 'panic_report.severity_id')
							->whereBetween('longitude', [(Float) Input::get('Longitude1'), (Float) Input::get('Longitude2')])
                            ->whereBetween('latitude', [(Float) Input::get('Latitude1'), (Float) Input::get('Latitude2')])
                            ->orderBy('panic_report.id', 'DESC')
                            ->get();
			
            foreach($panicReports as $row){
              if($row->Image){
                $row->Image = url('/laravel/public/') . '/'. $row->Image;
              }
            }

            // Send Response                 
            $response = new StdClass();
            $response->Status = 'Success';
            $response->NumRows = count($panicReports);
            $response->Datasource = $panicReports;
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

    // @method: POST
    // Login 
    public function postAction(Request $request)
    {
        try{
            // Get Token
            $Token = $request->header('Token');
            $session = ApiToken::get($Token);

             // Get Raw content
            $request_data = json_decode($request->getContent());
            $PanicTypeId = $request_data->PanicTypeId;
            $SeverityId = isset($request_data->SeverityId) ? $request_data->SeverityId : 1;
            $Message = $request_data->Message;
            $Longitude = $request_data->Longitude;
            $Latitude = $request_data->Latitude;
            $Timestamp = $request_data->Timestamp;
            
            // Get Driver Data from SIAB
            $cardDetail = SiabKartu::find($session->card_id);

            //get the base-64 from data
            $filename = NULL;
            if($request_data->Image){
			  // Auto detect Base64 or Data URL with Base64
              $comma_pos = strpos($request_data->Image, ",");
              $base64_str = $comma_pos === false ? $request_data->Image : substr($request_data->Image, $comma_pos + 1);

              //decode base64 string
              $image = base64_decode($base64_str);
              $filename = "panic-".time().".png";
              $path = public_path().'/'.$filename;
              file_put_contents($path, $image);
            }
			
            // Insert Row
            $panicReport = new PanicReport();
            $panicReport->card_id = $session->card_id;
            $panicReport->driver_id = $session->driver_id;
            $panicReport->panic_type_id = $PanicTypeId;
            $panicReport->severity_id = $SeverityId;
            $panicReport->driver_name = $cardDetail->NamaSupir;
            $panicReport->driver_phone = $cardDetail->NomorHandphone;
            $panicReport->longitude = $Longitude;
            $panicReport->latitude = $Latitude;
            $panicReport->message = $Message;
			      $panicReport->image = $filename;
            $panicReport->status = 'OPEN';
            $panicReport->created = date('Y-m-d H:i:s');

            $panicReport->save();

            // Send Reponse
            $response = new StdClass();
            $response->Status = 'Success';
            $response->panic_report_id = $panicReport->id;

            $eventName = new \Ntmc\Events\EventName();
            $eventName->data['driverID'] = $session->driver_id;
            $eventName->data['lat'] = $Latitude;
            $eventName->data['lng'] = $Longitude;
            $eventName->data['message'] = $Message;
            event($eventName);

            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

     // @method: PATCH
    // update Report 
    public function patchAction(Request $request)
    {
        try{
            // Get Token
            $Token = $request->header('Token');
            $session = ApiToken::get($Token);

             // Get Raw content
            $request_data = json_decode($request->getContent());
            $PanicReportId = $request_data->PanicReportId;
            $Status = $request_data->Status;

            // Get Existing Data
            $panicReport = PanicReport::find($PanicReportId);
            
            // Validations
            if(!$panicReport || $panicReport->driver_id != $session->driver_id){
                throw new Exception('Panic Report not found', 404);
            }

            if($panicReport->status == 'CLOSED'){
                throw new ApiBusinessException('Panic report already closed', 532);
            }

            if(!in_array($Status, ['OPEN', 'RESPONDED', 'CLOSED'])){
                throw new ApiBusinessException('Not valid status. Please use OPEN, RESPONDED or CLOSED', 525);
            }

            // Change Status
            $panicReport->status = $Status;
            $panicReport->save();

            // Send Reponse
            $response = new StdClass();
            $response->Status = 'Success';

            $eventName = new \Ntmc\Events\EventName();
            $eventName->data['driverID'] = $panicReport->driver_id;
            $eventName->data['lat'] = $panicReport->latitude;
            $eventName->data['lng'] = $panicReport->longitude;
            $eventName->data['message'] = $Status;
            event($eventName);

            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }
}
