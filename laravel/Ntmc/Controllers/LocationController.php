<?php

namespace Ntmc\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Ntmc\Models\DriverLocation;
use Ntmc\Models\CurrentDriverLocation;

class LocationController extends BaseController
{

  public function postUpdate(Request $request)
  {
    $res = DriverLocation::where('truck_id', $request['card_id'])
            ->where('driver_id', $request['id'])
            ->first();

    if ($res) {
      // update
      $res->latitude = $request['lat'];
      $res->longitude = $request['lng'];
      $res->save();

    } else {

      // insert
      $lastLocation= new DriverLocation();

      $lastLocation->truck_id = $request['card_id'];
      $lastLocation->driver_id = $request['id'];
      $lastLocation->latitude = $request['lat'];
      $lastLocation->longitude = $request['lng'];
      //$lastLocation->active = 1;
      $lastLocation->save();
    }

    $baseUrl = config('app.siabAPIBaseURL');

    $SIABCall = \Guzzle::get($baseUrl . "/siab/api/ntmc/check_data?app_id=siab03ntmc&card_id=" . htmlspecialchars($request['card_id'], ENT_QUOTES) . "&id=" . htmlspecialchars($request['id'], ENT_QUOTES));
    $bodyRespSIABCall = $SIABCall->getBody();

    $bodyRespSIABCall = json_decode($bodyRespSIABCall);

    /*if ( is_object($bodyRespSIABCall) && $bodyRespSIABCall->is_error === false ) {
      $factLocation = new CurrentDriverLocation();
      $factLocation->driver_id = $bodyRespSIABCall->payload->id;
      //$factLocation->card_id = $bodyRespSIABCall->payload->card_id;
      //$factLocation->company_name = $bodyRespSIABCall->payload->company_name;
      //$factLocation->driver_name = $bodyRespSIABCall->payload->driver_name;
      //$factLocation->plat_license = $bodyRespSIABCall->payload->plat_license;
      //$factLocation->insurance = $bodyRespSIABCall->payload->insurance;
      $factLocation->latitude = $request['lat'];
      $factLocation->longitude = $request['lng'];
      //$factLocation->active = 1;
      $factLocation->save();
    }*/


    $eventName = new \Ntmc\Events\EventName();
    $eventName->data['driverID'] = $request['id'];
    $eventName->data['lat'] = $request['lat'];
    $eventName->data['lng'] = $request['lng'];
    event($eventName);

    $response = ['is_error' => false, 'status_code' => 1, 'status_msg' => "Berhasil", 'payload' => "Ok"];

    return response()->json($response);
  }
}
