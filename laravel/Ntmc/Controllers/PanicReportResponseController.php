<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Ntmc\Models\User;
use Validator;
// use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Exception;
use Ntmc\Library\ApiToken;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\PanicReport;
use Ntmc\Models\PanicReportResponse;
use App\Exceptions\ApiBusinessException;

class PanicReportResponseController extends BaseController
{
    public function __construct()
    {
        //DISABLED: $this->middleware($this->guestMiddleware(), ['except' => ['logout','getLogout']]);
    }

    // @method: POST
    // New Report 
    public function postAction(Request $request){
        try{
            $panicReport = PanicReport::find($request->input('PanicReportId'));
            
            if(!$panicReport){
                throw new Exception('Panic Report not found', 404);
            }

            // Insert Row
            $panicResponse = new PanicReportResponse();
            $panicResponse->panic_report_id = $request->input('PanicReportId');
            $panicResponse->users_id = 0;
            $panicResponse->response = $request->input('Response');
            $panicResponse->created = date('Y-m-d H:i:s');

            $panicResponse->save();

            // Send Reponse
            $response = new StdClass();
            $response->Status = 'Success';
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

   
}
