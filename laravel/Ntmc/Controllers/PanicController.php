<?php

namespace Ntmc\Controllers;

//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;

use Ntmc\Models\LogPanic;
use Ntmc\Models\Panic;
use Ntmc\Models\ApiResponseBase;

class PanicController extends BaseController
{

  public function index(Request $request)
  {
    $querystring = $this->_buildQueryString($request);
    $panics = Panic::Pagination();

    return View('panics.index', compact('panics', 'querystring'));
  }

  public function create()
  {
    $panics = Panic::Pagination();
    //dd($panics);
    return view('panics.create', compact('panics'));
  }

  public function store(Request $request)
  {
    $this->validate($request, [
        'name' => 'required|unique:panic,name',
    ], [
        'required' => 'Kolom :attribute diperlukan!',
        'unique' => ':attribute sudah ada yang punya.'
    ]);

    //$request->merge(['active' => 1]);
    $panic = Panic::create($request->all());

    \Flash::success('Tipe Panik tersimpan.');
    return redirect()->route('panics.edit', [$panic->id]);
  }

  public function show($id)
  {
    $item = Panic::findOrFail($id);
    return view('panics.show', compact('item'));
  }

  public function edit($id)
  {
    $panics = Panic::Pagination();
    $item = Panic::findOrFail($id);

    return view('panics.edit', compact('panics', 'item'));
  }

  public function update(Request $request, $id)
  {
    $panic = Panic::findOrFail($id);

    $this->validate($request, [
        'name' => 'required|unique:panic,name,' . $panic->id,
    ], [
        'required' => 'Kolom :attribute diperlukan!',
        'unique' => ':attribute sudah ada yang punya.'
    ]);

    $panic->update($request->all());

    \Flash::success('Tipe Panik diperbaharui.');
    return redirect()->back();
  }

  public function destroy($id)
  {
    Panic::findOrFail($id)->delete();

    \Flash::success('Tipe Panik berhasil dihapus.');
    return redirect()->back();
  }

  public function getAll()
  {
    try{
      //$panics = Panic::select('id', 'name')->get();
      $panics = Panic::AllActive();

      // Send Response
      $response = new \StdClass();
      $response->Status = 'Success';
      $response->NumRows = count($panics);
      $response->Datasource = $panics;

      echo (new ApiResponseBase())->fillResponse($response);
    }catch(Exception $e) {
      echo (new ApiResponseBase())->fillException($e);
    }
  }

  public function postAlert(Request $request)
  {
    $logPanic= new LogPanic();

    $logPanic->card_id = $request['card_id'];
    $logPanic->driver_id = $request['id'];
    $logPanic->lat = $request['lat'];
    $logPanic->lng = $request['lng'];
    $logPanic->panic_id = $request['panic_id'];
    $logPanic->panic_id = $request['panic_id'];
    $logPanic->msg = $request['msg'];
    $logPanic->level = $request['level'];
    $logPanic->active = 1;
    /*$logPanic->created = $request['level'];
    $logPanic->updated = $request['level'];*/

    $logPanic->save();

    //dd($logPanic);
    $response = ['is_error' => false, 'status_code' => 1, 'status_msg' => "Berhasil", 'payload' => ['log_id' => $logPanic->id, 'status' => "Ok"] ];
    return response()->json($response);
  }

  public function postDone(Request $request)
  {
    $res = LogPanic::NotDone()
            ->where('card_id', $request['card_id'])
            ->where('driver_id', $request['id'])
            ->where('id', $request['log_id'])
            ->first();

    if ($res) {
      $res->active = 0;
      $res->updated = date('Y-m-d H:i:s');
      $res->save();
      $response = ['is_error' => false, 'status_code' => 1, 'status_msg' => "Berhasil", 'payload' => "Ok"];
    } else {
      $response = ['is_error' => true, 'status_code' => 404, 'status_msg' => "Log Tidak Tersedia"];
    }

    return response()->json($response);
  }



  //PRIVATE methods
  private function _buildQueryString($request)
  {
    $querystring = null;

    if( $request->has('type') ) {
      $querystring['type'] = $request->has('type') ? $request->type : '';
    } elseif($request->has('q')) {
      $querystring['q'] = $request->has('q') ? $request->q : '';
    }

    return $querystring;
  }
  //END PRIVATE methods
}
