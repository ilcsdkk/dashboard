<?php
/**
*   @package    User Controller
*   @author     kb.hartanto@gmail.com
*   @since      v1 - 01-06-2016
**/
namespace Ntmc\Controllers;

use Auth;

class AdminController extends BaseController
{

    //protected $layout = 'sites.layouts.default';
    /* Class Constructor */
    function __construct()
    {
       parent::__construct();
    }
    /**/
    public function getIndex()
    {
       return View('admin.index');
    }
}
