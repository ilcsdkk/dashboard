<?php

/**
*   @package    Custom Auth Controller
*   @author     kb.hartanto@gmail.com
*   @since      v1 - 01-06-2016
**/

namespace Ntmc\Controllers;
use Ntmc\Models\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Mail;
use Crypt;
use Flash;

class AuthController extends BaseController
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/dashboard';
    protected $redirectAfterLogout = '/auth/login';
    protected $guard = 'web';
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => ['getLogout']]);
    }

    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'user_first_name' => $data['name'],
            'user_email' => $data['email'],
            'user_password' => bcrypt($data['password']),
        ]);
    }*/

    public function getLogin(Request $request)
    {
        $back = $request->get('back');
        return View('sites.auth.login')->with(compact('back'));
    }

    public function getRegister()
    {
        return View('sites.auth.register');
    }

    public function getForgot()
    {
        return view('sites.auth.forgot');
    }

    public function getVeritication()
    {
        return View('sites.auth.veritication');
    }



    //custom auth from here
    public function postRegister(Request $request)
    {
         $rules = [
            'name' => 'required|min:6',
            'email' => 'required|email|unique:user',
            'password' => 'required|confirmed|min:6'
        ];

        $input = $request->only(
            'name',
            'email',
            'password',
            'password_confirmation'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator);
        }

        $confirmation_code = str_random(30);
        $password_key = str_random(20);

        User::create([
            'first_name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'confirmation_code' => $confirmation_code,
            'password_key' => $password_key,
            'status' => 2,
            'role_id' => 4,
        ]);

        $data['confirmation_code'] = $confirmation_code;

        Mail::send('emails.verify', $data, function($message) use ($request) {
            $message->to($request->get('email'), $request->get('name'))
                ->subject('Verify your email address');
        });

        Flash::message('Thanks for signing up! Please check your email.');

        return redirect()->home();
    }

    public function getVerify($confirmation_code='')
    {
         if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeDatabseException;
        }

        $user->status = '1';
        $user->confirmation_code = null;
        $user->save();

        flash()->success('You have successfully verified your account.');

        return redirect()->route('auth.login');
    }

    public function postLogin(Request $request,$back='')
    {
        $rules = [
            'username' => 'required|max:255|exists:users',
            //'password' => 'required|min:6|confirmed',
            'password' => 'required|min:6',
        ];

        $input = $request->only('username', 'password');

        $validator = Validator::make($input, $rules);
        if($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);


        $credentials = [
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ];

        if ( ! Auth::attempt( $credentials )) {

            return redirect()->back()
                ->withInput()
                ->withErrors([
                    'credentials' => 'We were unable to sign you in.'
                ]);

        } else {

            flash()->success('Welcome!');
            //if ($back) return redirect()->to(Crypt::decrypt($back));
            return redirect()->intended('dashboard');
        }

        /*$userExisting = User::where($credentials)->firstOrFail();
        if ( ! $userExisting )
        {
            return redirect()->back()
                ->withInput()
                ->withErrors([
                    'credentials' => 'We were unable to sign you in.'
                ]);
        } else {
            //now check the password validity
            $status = password_verify($request->get('password'), $userExisting->password);
            if ( $status ) {
                flash()->success('Welcome!');

                return redirect()->home();
                //return redirect()->intended('dashboard');
            }

        }*/

    }
    //end custom auth

}
