<?php

namespace Ntmc\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller {

  function __construct()
  {
    $this->default_var();
  }

  private function default_var()
  {
    $site_title = 'Ntmc';
    $site_name = 'Admin Panel';
    $site_name_mini = 'Spanel';
    //share default variable
    view()->share(compact('site_title','site_name','site_name_mini'));
  }
}
