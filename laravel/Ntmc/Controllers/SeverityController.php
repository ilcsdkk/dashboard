<?php

namespace Ntmc\Controllers;

//use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;

use Ntmc\Models\LogPanic;
use Ntmc\Models\Severity;
use Ntmc\Models\ApiResponseBase;

class SeverityController extends BaseController
{
  public function getAll()
  {
    try{
	  $severities = DB::table('severity')
	  				->select([
						'id AS SeverityId',
						'severity AS Severity'
					])
					->get();

      // Send Response
      $response = new \StdClass();
      $response->Status = 'Success';
      $response->NumRows = count($severities);
      $response->Datasource = $severities;

      echo (new ApiResponseBase())->fillResponse($response);
    }catch(Exception $e) {
      echo (new ApiResponseBase())->fillException($e);
    }
  }

}
