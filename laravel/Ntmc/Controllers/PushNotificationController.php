<?php

namespace Ntmc\Controllers;

use Illuminate\Http\Request;
use PushNotif;

class PushNotificationController extends BaseController
{
    public function sendNotificationToDevice()
    {
        $deviceToken = 'APA91bEqfNz0IWEok_ZgcY1zQB43kZ_ZNFtpuAUp7gz97BipYBi2atnyhtYqGitE3bTH0b7bbYysXuyEbKYVFNYQqv-UlWmH-OO5DAtLMk9pieIPrAYWDo8';

        $message = [];
        $message['type'] = "kecelakan";
        $message['latitude'] = -6.211835;
        $message['longitude'] = 106.844912;
        $message['description'] = "We have successfully sent a push notification!";
        $message['timestamp'] = "1470577893";
        $message['to'] = "APA91bEqfNz0IWEok_ZgcY1zQB43kZ_ZNFtpuAUp7gz97BipYBi2atnyhtYqGitE3bTH0b7bbYysXuyEbKYVFNYQqv-UlWmH-OO5DAtLMk9pieIPrAYWDo8";

        // Send the notification to the device with a token of $deviceToken
        $collection = PushNotif::app('appNameAndroid')
            ->to($deviceToken)
            ->send($message);

        dd($collection);
    }
}
