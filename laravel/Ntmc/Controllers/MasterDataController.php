<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Exception;
use Ntmc\Library\ApiToken;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\PanicType;
use App\Exceptions\ApiBusinessException;

class MasterDataController extends BaseController
{
    public function __construct()
    {
        //DISABLED: $this->middleware($this->guestMiddleware(), ['except' => ['logout','getLogout']]);
    }

    // @method: GET
    // Get All Masterdata 
    public function getAction(Request $request){
        try{
             // Get Token
            $Token = $request->header('Token');
            $session = ApiToken::get($Token);

            // Get Location Markers
            $panicReports = DB::table('panic_report')
                            ->select(
                                'card_id AS CardId',
                                'driver_id AS DriverId', 
                                'truck_id AS TruckId', 
                                'message AS Message', 
                                'longitude AS Longitude', 
                                'latitude AS Latitude', 
                                'created AS Created'
                            )
                            ->whereBetween('longitude', [(Float) Input::get('Longitude1'), (Float) Input::get('Longitude2')])
                            ->whereBetween('latitude', [(Float) Input::get('Latitude1'), (Float) Input::get('Latitude2')])
                            ->get();
    
            // Send Response                 
            $response = new StdClass();
            $response->Status = 'Success';
            $response->NumRows = count($panicReports);
            $response->Datasource = $panicReports;
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }
}
