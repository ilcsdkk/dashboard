<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\SiabKartu;
use Ntmc\Models\SiabSupir;
use Ntmc\Library\ApiToken;

class DriverController extends BaseController
{
    public function __construct()
    {
        //DISABLED: $this->middleware($this->guestMiddleware(), ['except' => ['logout','getLogout']]);
    }

    public function getAction(Request $request){
        try{
            // Get Logon Session

            // Get Card Detail from SIAB
            $cardDetail = SiabKartu::find(Input::get('CardId'));
    
            // Send Response                 
            $response = new StdClass();
            $response->Status = 'Success';
            $response->Datasource = $cardDetail;
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

    // @method: POST
    // Login 
    public function postAction(Request $request){
        try{
            // Get Raw content
            $request_data = json_decode($request->getContent());
            $CardId = $request_data->CardId;
            $CellphoneNumber = $request_data->CellphoneNumber;

            // Validation
            if($CellphoneNumber == ''){
                throw new Exception('Invalid phone number', 502);
            }

            // Get Driver Data from SIAB
            $cardDetail = SiabKartu::find($CardId);
            
            // Only proceed if Phone Number not set
            if($cardDetail->NomorHandphone != ''){
                throw new Exception('Cannot update phone number. Phone number already set.', 302);
            }
            
            // Generate one time password (PIN)
            $pin = rand(1000, 9999);

            // Create session and token
            $token = ApiToken::create($cardDetail->SupirId, $CardId, $pin);

            // Send Notification
            SiabSupir::save($cardDetail->SupirId, $CellphoneNumber, $pin);
            
            // Create Response
            $response = new StdClass();
            $response->Status = 'Success';
            $response->DriverId = $cardDetail->SupirId;
            $response->TokenKey = $token;

            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }
}
