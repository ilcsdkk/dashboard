<?php
/**
*   @package    User Controller
*   @author     kb.hartanto@gmail.com
*   @since      v1 - 01-06-2016
**/
namespace Ntmc\Controllers;

use Ntmc\Models\User;
use Illuminate\Http\Request;
//use Illuminate\Routing\Router;
//use Route;

class UserController extends BaseController
{

    protected $layout = 'sites.layouts.default';
    /* Class Constructor */
    function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $querystring = $this->_buildQueryString($request);
        $users = User::Pagination($request);

        return view('users.index', compact('users', 'querystring'));
    }

    public function create(Request $request)
    {
        //dd(Route::currentRouteName());
        $users = User::Pagination($request);

        return view('users.create', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username',
            'phone_number' => 'required|unique:users,phone_number',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'role' => 'required'
        ], [
            'required' => 'Kolom :attribute diperlukan!',
            'exists' => 'Kolom :attribute tidak ditemukan!',
            'email' => 'Kolom :attribute harus berupa email.',
            'unique' => ':attribute sudah ada yang punya.'
        ]);

        $data = $request->all();

        $user = User::create($data);

        /*$user->usermeta()->create([
            'picture' => 'icon-user-default.png',
            'cover' => 'cover-default.jpg'
        ]);*/

        \Flash::success('User tersimpan.');
        return redirect()->route('users.edit', [$user->id]);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('user.show', compact('user'));
    }

    public function edit(Request $request, $id)
    {
        $users = User::Pagination($request);
        $user = User::findOrFail($id);

        return view('users.edit', compact('users', 'user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'username' => 'required|unique:users,username,' . $user->id,
            'phone_number' => 'required|unique:users,phone_number,' . $user->id,
            'email' => 'required|email|unique:users,email,' . $user->id,
            'role' => 'required'
            /*'username' => 'required',
            'phone_number' => 'required',
            'email' => 'required|email'*/
        ], [
            'required' => 'Kolom :attribute diperlukan!',
            'exists' => 'Kolom :attribute tidak ditemukan!',
            'email' => 'Kolom :attribute harus berupa email.'
        ]);

        $user->update($request->all());

        \Flash::success('User diperbaharui.');
        return redirect()->back();
    }

    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        \Flash::success('User berhasil dihapus.');
        return redirect()->back();
    }



    //PRIVATE methods
    private function _buildQueryString($request)
    {
        $querystring = null;

        if( $request->has('type') ) {
            $querystring['type'] = $request->has('type') ? $request->type : '';
        } elseif($request->has('q')) {
            $querystring['q'] = $request->has('q') ? $request->q : '';
        }

        return $querystring;
    }
    //END PRIVATE methods
}
