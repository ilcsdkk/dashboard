<?php

/**
*   @package    Driver Data Controller for Mobile Device API
*   @author     djati.satria@gmail.com
*   @since      v1 - 2016 08 24
**/

namespace Ntmc\Controllers;

use Ntmc\Models\User;
use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use Crypt;
use Flash;
use StdClass;
use Exception;
use Ntmc\Models\ApiResponseBase;
use Ntmc\Models\DriverLocation;
use Ntmc\Models\CurrentDriverLocation;
use Ntmc\Library\ApiToken;
use App\Exceptions\ApiBusinessException;


class TruckLocationController extends BaseController
{
    public function __construct()
    {
        
    }

    // @method: GET
    // Get All updated location data
    public function getAction(Request $request)
    {
        try{
            // Get Logon Session

            // Get Location Markers
            $locations = DB::table('current_driver_location')
                ->select('current_driver_location.driver_id AS DriverId', 'current_driver_location.card_id AS CardId', 'current_driver_location.truck_id AS TruckId', 'longitude AS Longitude', 'latitude AS Latitude', 'updated AS LastUpdate')
                ->join('api_session', 'current_driver_location.driver_id', '=', 'api_session.driver_id')
                ->whereRaw('`longitude` between least(?, ?) and greatest(?, ?)
AND `latitude` between least(?, ?) and greatest(?, ?)', [(Float) Input::get('Longitude1'), (Float) Input::get('Longitude1'), (Float) Input::get('Longitude2'), (Float) Input::get('Longitude2'), (Float) Input::get('Latitude1'), (Float) Input::get('Latitude1'), (Float) Input::get('Latitude2'), (Float) Input::get('Latitude2')])
                ->get();

            // Send Response                 
            $response = new StdClass();
            $response->Status = 'Success';
            $response->NumRows = count($locations);
            $response->Datasource = $locations;
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }

    // @method: POST
    // Post From Mobile Device 
    public function postAction(Request $request){
        try{
            // Get Token
            $Token = $request->header('Token');
            $session = ApiToken::get($Token);

            // Get Raw content
            $request_data = json_decode($request->getContent());
            $Longitude = $request_data->Longitude;
            $Latitude = $request_data->Latitude;
            $Timestamp = $request_data->Timestamp;

            // Save Location for History
            $driverLocation = new DriverLocation();
            $driverLocation->driver_id = $session->driver_id;
            $driverLocation->card_id = $session->card_id;
            $driverLocation->longitude = $Longitude;
            $driverLocation->latitude = $Latitude;
            $driverLocation->created = date('Y-m-d H:i:s');
            $driverLocation->save();

            // Current Location for Dashboard Use
            $currentDriverLocation = CurrentDriverLocation::find($session->driver_id);
            if(!$currentDriverLocation){
                // Create model if not exists
                $currentDriverLocation = new CurrentDriverLocation();
            }

            // Update to latest data
            $currentDriverLocation->driver_id = $session->driver_id;
            $currentDriverLocation->card_id = $session->card_id;
            $currentDriverLocation->truck_id = '';
            $currentDriverLocation->longitude = $Longitude;
            $currentDriverLocation->latitude = $Latitude;
            $currentDriverLocation->updated = date('Y-m-d H:i:s');
            $currentDriverLocation->save();

            // Send Response                 
            $response = new StdClass();
            $response->Status = 'Success';

            //update markers event
            /*$eventName = new \Ntmc\Events\EventName();
            $eventName->data['driverID'] = $session->driver_id;
            $eventName->data['lat'] = $Latitude;
            $eventName->data['lng'] = $Longitude;
            event($eventName);*/
            
            echo (new ApiResponseBase())->fillResponse($response);
        }catch(Exception $e){
            echo (new ApiResponseBase())->fillException($e);
        }
    }
}
