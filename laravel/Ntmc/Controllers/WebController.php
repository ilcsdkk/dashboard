<?php
namespace Ntmc\Controllers;

use App;
use DB;
use Illuminate\Http\Request;
use Log;
use Auth;
use Ntmc\Models\ApiSession;


class WebController extends BaseController
{
  function __construct()
  {
    $this->default_var();
  }

  public function getIndex()
  {
    if(Auth::check()) {
      return redirect()->intended('/dashboard');
    }
    else
      return redirect()->intended('/auth/logout');
  }

  public function getDashboard()
  {
    $activeTrucks = ApiSession::with([
        'currentLocation',
    ])->get();
    //dd($activeTrucks); exit;

    return view('sites.index2', compact('activeTrucks'));
  }

  public function getRestCall()
  {
    //$resp = Festivus::client('collections/5d535f5524357176844b');
    //$resp = Festivus::client('siab');
    $response = \Guzzle::get("https://www.getpostman.com/collections/5d535f5524357176844b");

    //dd($response->getBody()); exit(' !test! ');
    //dd($response->getStatusCode()); exit(' !test! ');
    //dd($response->getHeader('content-type'));

    dd(json_decode($response->getBody()));
  }

  private function default_var()
  {
    $site_title = 'NTMC';
    $site_name = 'Admin Panel';
    $site_name_mini = 'Spanel';
      //share default variable
    view()->share(compact('site_title','site_name','site_name_mini'));
  }

  public function updateLocation(Request $request)
  {
    $resp = [];
    $data = json_decode($request->getContent());
    foreach ($data as $idx => $entity) {
      $resp[$idx]['id'] = $entity->id;
      $resp[$idx]['lat'] = $entity->myLatLng->lat + 0.01;
      $resp[$idx]['lng'] = $entity->myLatLng->lng + 0.01;
    }
    echo json_encode($resp);
  }

}
