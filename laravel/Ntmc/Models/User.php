<?php
namespace Ntmc\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    //use SoftDeletes;

    protected $table = 'users';
    protected $primaryKey  = 'id';
    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'phone_number', 'created_at', 'update_at', 'confirmation_code', 'password_key', 'status', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'email','password', 'remember_token',
    ];

    protected static function boot()
    {
        static::creating(function ($model) {
            //default password
            $model->password = bcrypt( $model->password );

            return true;
        });
    }

    public function getTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function hasRole($name)
    {
        if(is_array($name)) {
            foreach ($name as $roleName) {
                if( $this->role === $roleName ) {
                    return true;
                }
            }
        } else{
            if( $this->role === $name ) {
                return true;
            }
        }
        return false;
    }

    public function haveRoles($roles)
    {
        // Check if the user is a root account
        if($this->role == 'admin') {
            return true;
        }

        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }

    public function scopePagination($query, $request)
    {
        if( $request->has('type') ) {
            $users = $query->where('role', $request->type);

        } elseif( $request->has('q') ){
            $users = $query->where(function($query) use ($request){
                $query->where('username', 'LIKE', '%' . $request->q . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->q . '%');
            })->orderBy('created_at', 'DESC');
        } else {
            $users = $query;
        }

        return $users->paginate(5);
    }



    //PRIVATE methods
    private function getUserRole()
    {
        return $this->role;
    }

    private function checkIfUserHasRole($need_role)
    {
        return ( strtolower($need_role)==strtolower($this->role) ) ? true : false;
    }
    //END PRIVATE methods
}
