<?php
namespace Ntmc\Models;

class PosPolisi extends BaseModel
{
  protected $table = 'pos_polisi';

  protected $fillable = [
    'nama', 'lat', 'long', 'jenis_pos_polisi_id', 'parent', 'alamat', 'contact_person'
  ];

  public function jenisPos()
  {
      return $this->belongsTo('\Ntmc\Models\JenisPosPolisi', 'id', 'id');
  }

    public function scopePagination($query, $jenis_pos_polisi = '')
    {
        $res = $query->orderBy( $this::CREATED_AT, 'DESC');

        if( $jenis_pos_polisi != '' ) {
            $res = $query->where('jenis_pos_polisi_id', $jenis_pos_polisi)->orderBy( $this::CREATED_AT, 'DESC');
        }

        return $res->paginate(5);
    }
}
