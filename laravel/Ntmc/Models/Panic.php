<?php

namespace Ntmc\Models;


class Panic extends BaseModel
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'panic';
    protected $primaryKey = 'id';
    //public $timestamps = false;

    protected $fillable = [
        'name', 'deskripsi', 'active'
    ];

    public function scopeAllActive($query)
    {
        $res = $query->select('id', 'name')->where('active', 1);
        return $res->get();
    }

    public function scopePagination($query)
    {
        $res = $query->orderBy( $this::CREATED_AT, 'DESC');
        return $res->paginate(5);
    }

    protected static function boot()
    {
        static::creating(function ($model) {
            //default password
            $model->active = 1;

            return true;
        });
    }
}
