<?php

namespace Ntmc\Models;

use GuzzleHttp\Client;
use Exception;

class SiabKartu
{
    public static function find($CardId)
    {
        $baseUrl = config('app.siabAPIBaseURL');
        $client = new Client();

        $res = $client->get($baseUrl . '/siab/api/ntmc/kartu', [
            'query' => [
                'app_id' => 'siab03ntmc', 
                'card_id' => $CardId
            ]
        ]);
        if($res->getStatusCode() != '200'){
            throw new Exception('Failed to contact SIAB', 202);
        }
        
        $siabResponse = json_decode($res->getBody());
        
        if($siabResponse->is_error == true){
            throw new Exception('SIAB ERROR: ' . $siabResponse->status_msg, $siabResponse->status_code);
        }

        return $siabResponse->payload;
    }
    
}
