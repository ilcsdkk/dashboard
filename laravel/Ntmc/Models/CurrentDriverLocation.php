<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;


class CurrentDriverLocation extends Model
{
    protected $table = 'current_driver_location';
    protected $primaryKey  = 'driver_id';
    public $timestamps = false;

    public function session()
    {
        return $this->hasOne('Ntmc\Models\ApiSession', 'driver_id', 'driver_id');
    }
}
