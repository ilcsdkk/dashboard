<?php

namespace Ntmc\Models;

use GuzzleHttp\Client;
use Exception;

class SiabSupir
{    
    public static function save($driver_id, $phone_number, $pin)
    {
        $baseUrl = config('app.siabAPIBaseURL');
        $client = new Client();

        $res = $client->post($baseUrl . '/siab/api/ntmc/supir', [
            'body' => [
                'app_id' => 'siab03ntmc', 
                'SupirId' => $driver_id, 
                'NomorHandphone' => $phone_number, 
                'VerCode' => $pin,
            ]
        ]);
        if($res->getStatusCode() != '200'){
            throw new Exception('Failed to contact SIAB', 202);
        }
        
        $siabResponse = json_decode($res->getBody());

        if($siabResponse->is_error == true){
            throw new Exception('SIAB ERROR: ' . $siabResponse->status_msg, $siabResponse->status_code);
        }

        return $siabResponse;
    }
}
