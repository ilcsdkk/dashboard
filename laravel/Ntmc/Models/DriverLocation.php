<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;


class DriverLocation extends Model
{
    protected $table = 'driver_location';
    protected $primaryKey  = 'id';
    public $timestamps = false;
    
}
