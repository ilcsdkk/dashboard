<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;


class ApiSession extends Model
{
    protected $table = 'api_session';
    protected $primaryKey  = 'token';
    public $timestamps = false;
    public $incrementing = false;

    public function currentLocation()
    {
        return $this->hasOne('Ntmc\Models\CurrentDriverLocation', 'driver_id', 'driver_id');
    }
}
