<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {


    protected $table = 'user_role';

    public function users()
    {
        return $this->hasMany('\Ntmc\Models\User', 'role_id', 'id');
    }
}
