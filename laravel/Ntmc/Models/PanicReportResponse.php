<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;


class PanicReportResponse extends Model
{
    protected $table = 'panic_report_response';
    protected $primaryKey  = 'id';
    public $timestamps = false;
    
}
