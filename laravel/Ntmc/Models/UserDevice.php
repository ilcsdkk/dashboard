<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'user_device';
    protected $primaryKey  = 'driver_id';
    public $timestamps = true;
}
