<?php
/**
**  @models ApiResponseBase
** @author djati.satria@gmail.com
**/

namespace Ntmc\Models;

class ApiResponseBase {
    public $isSuccess;
    public $statusCode;
    public $statusMessage;
    public $payload;

    public function __construct(){
        $this->payload = NULL;
    }

    public function toJson(){
        return json_encode($this);
    }

    public function fillResponse($response){
        $this->isSuccess = true;
        $this->statusCode = '0';
        $this->statusMessage = '';
        $this->payload = $response;

        return $this->toJson();
    }

    public function fillException($exception){
        $this->isSuccess = false;
        $this->statusCode = $exception->getCode();
        $this->statusMessage = $exception->getMessage();

        return $this->toJson();
    }
}
