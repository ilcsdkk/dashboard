<?php
namespace Ntmc\Models;


class JenisPosPolisi extends BaseModel
{
  protected $table = 'jenis_pos_polisi';

  protected $fillable = [
    'nama'
  ];

  public function posPolisi()
  {
      return $this->hasMany('\Ntmc\Models\PosPolisi', 'jenis_pos_polisi_id', 'id');
  }

  public function scopePagination($query)
  {
    $res = $query->orderBy( $this::CREATED_AT, 'DESC');

    return $res->paginate(5);
  }

}
