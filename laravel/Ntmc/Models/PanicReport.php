<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;


class PanicReport extends Model
{
    protected $table = 'panic_report';
    protected $primaryKey  = 'id';
    public $timestamps = false;
    
}
