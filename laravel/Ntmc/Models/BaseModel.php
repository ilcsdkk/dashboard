<?php
/**
**  @models BaseModel
** @author kb.hartanto@gmail.cm
**/

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

//use SoftDeletes;

class BaseModel extends \Eloquent {

  //use SoftDeletes;

  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';
  const DELETED_AT = 'deleted';

  public function getTableColumns()
  {
    return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
  }

}
