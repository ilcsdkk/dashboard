<?php

namespace Ntmc\Models;

use Illuminate\Database\Eloquent\Model;


class BroadcastMessage extends Model
{
    protected $table = 'broadcast_message';
    protected $primaryKey  = 'id';
    public $timestamps = false;
    
}
