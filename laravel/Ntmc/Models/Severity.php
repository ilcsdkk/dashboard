<?php

namespace Ntmc\Models;


class Severity extends BaseModel
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    protected $table = 'severity';
    protected $primaryKey = 'id';

    protected $fillable = [
        'severity'
    ];

    public function scopeAllActive($query)
    {
        $res = $query->select('id', 'severity')->where('active', 1);
        return $res->get();
    }

    public function scopePagination($query)
    {
        $res = $query->orderBy( $this::CREATED_AT, 'DESC');
        return $res->paginate(5);
    }
}
