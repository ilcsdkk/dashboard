<?php
/*
*
*/

Route::get('/',['as' => 'home','uses'=>'WebController@getIndex']);
//Route::match(['get', 'post'], '/',['as' => 'home','uses'=>'WebController@getIndex']);

//Route::get('/default',['as' => 'default', 'uses'=>'WebController@getHome']);

/* Auth default*/
Route::controller('auth','AuthController');
Route::post('/auth/login', ['uses' => 'AuthController@postLogin', 'as' => 'auth.login']);

Route::group(['middleware' => 'auth'], function() {

    Route::get('/dashboard', ['as' => 'home', 'uses' => 'WebController@getDashboard']);

    Route::resource('/pos-polisi','PosPolisiController');
    Route::resource('/jenis-pos-polisi','JenisPosPolisiController');
    Route::resource('/users','UserController');

    Route::resource('/panics','PanicController');
});

// user dashboard need login if access it


Route::group(['prefix' => 'admin', 'middleware' => ['auth','roles'], 'roles' => ['root','staff']], function() {
    //
  Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminController@getIndex']);

  // master data crud
  Route::get('master/table/{table_name}/create', ['as' => 'master.table.create', 'uses' => 'MasterController@getCreateTable']);
});

//Route::get('/test-rest-call',['as' => 'home','uses'=>'WebController@getRestCall']);
Route::resource('/updateLocation', 'WebController@updateLocation');



// API routes...
Route::group(['prefix' => 'api/v1'], function() {
    // panic
    Route::post('panic/alert', ['uses' => 'PanicController@postAlert']);
    Route::post('panic/done', ['uses' => 'PanicController@postDone']);
    Route::post('update_location',['uses' => 'LocationController@postUpdate']);
    Route::get('notification/', 'PushNotificationController@sendNotificationToDevice');
});

// New API
Route::group(['prefix' => 'device_api/v1'], function(){
    Route::get('master_data', ['uses' => 'MasterDataController@getAction']);
    Route::get('panic_types', ['uses' => 'PanicController@getAll']);
    Route::get('severity', ['uses' => 'SeverityController@getAll']);
    
    Route::post('login', ['uses' => 'DeviceAuthController@login']);
    Route::post('authorize', ['uses' => 'DeviceAuthController@authorizeToken']);
    Route::post('logout', ['uses' => 'DeviceAuthController@logout']);

    Route::post('driver', ['uses' => 'DriverController@postAction']);
    Route::post('device_id', ['uses' => 'DeviceController@postAction']);
    Route::post('truck_location', ['uses' => 'TruckLocationController@postAction']);
    Route::post('panic_report', ['uses' => 'PanicReportController@postAction']);
    Route::patch('panic_report', ['uses' => 'PanicReportController@patchAction']);
});

Route::group(['prefix' => 'dashboard_api/v1'], function(){
    Route::get('truck_location', ['uses' => 'TruckLocationController@getAction']);
    Route::get('driver', ['uses' => 'DriverController@getAction']);
    Route::post('broadcast_message', ['uses' => 'BroadcastMessageController@postAction']);
    Route::get('panic_report', ['uses' => 'PanicReportController@getAction']);
    Route::post('panic_report_response', ['uses' => 'PanicReportResponseController@postAction']);

    Route::post('alert_notif', ['uses' => 'DeviceController@broadcastAlert']);
});

Route::group(['prefix' => 'api'], function(){
    Route::get('pos_polisi_position', ['uses' => 'PosPolisiController@getPosition']);
});