<?php
namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class DetailViews extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
        $allColumns = $this->config['model']->getTableColumns();

        return view("widgets.eloquent_detail_views", [
            'model' => $this->config['model'],
            'allColumns' => $allColumns
        ]);
    }
}