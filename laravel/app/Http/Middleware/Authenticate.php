<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = '';   
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                //dd($request->path());
                return redirect()->guest('auth/login'.'?back='.\Crypt::encrypt($request->path()));
            }
        } else { 
            // current user active
            $user = $request->user();
        }
        
        if ($user) View()->share('userSession', $user);
        
        return $next($request);
    }
}
