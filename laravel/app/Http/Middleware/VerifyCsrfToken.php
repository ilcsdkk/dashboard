<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/api/v1/panic/alert',
        '/api/v1/panic/done',
        '/api/v1/update_location',
        '/api/v1/register_gcm',

        // API V2
        '/device_api/v1/*',
        '/dashboard_api/v1/*'
    ];

  

}
