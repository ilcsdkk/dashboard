<?php

namespace App\Providers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // set default path for skolink apps
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }   
}
