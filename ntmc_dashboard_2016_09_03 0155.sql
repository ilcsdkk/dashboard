-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Trucks`
--

DROP TABLE IF EXISTS `Trucks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Trucks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_polisi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Trucks`
--

LOCK TABLES `Trucks` WRITE;
/*!40000 ALTER TABLE `Trucks` DISABLE KEYS */;
/*!40000 ALTER TABLE `Trucks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_session`
--

DROP TABLE IF EXISTS `api_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_session` (
  `token` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `pin` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  `expired` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`token`),
  KEY `api_session_driver_id_index` (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_session`
--

LOCK TABLES `api_session` WRITE;
/*!40000 ALTER TABLE `api_session` DISABLE KEYS */;
INSERT INTO `api_session` VALUES ('roech2','19','0000000019102','0','1000',1,'2016-09-18 10:31:12','2016-09-04 23:54:00'),('token','20','0000000019101','1','1111',1,'2016-09-26 15:21:12','2016-09-04 23:54:38');
/*!40000 ALTER TABLE `api_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `broadcast_message`
--

DROP TABLE IF EXISTS `broadcast_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `broadcast_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `broadcast_type_id` int(11) NOT NULL,
  `title` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(8,5) NOT NULL,
  `latitude` decimal(8,5) NOT NULL,
  `broadcast_range_meter` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `broadcast_message`
--

LOCK TABLES `broadcast_message` WRITE;
/*!40000 ALTER TABLE `broadcast_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `broadcast_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `current_driver_location`
--

DROP TABLE IF EXISTS `current_driver_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current_driver_location` (
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(8,5) NOT NULL,
  `latitude` decimal(8,5) NOT NULL,
  `heading` double(8,2) NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `current_driver_location`
--

LOCK TABLES `current_driver_location` WRITE;
/*!40000 ALTER TABLE `current_driver_location` DISABLE KEYS */;
INSERT INTO `current_driver_location` VALUES ('19','0000000019102','0',106.84000,-6.18000,1.00,'2016-09-05 00:06:04'),('20','0000000019101','1',106.83000,-6.17000,1.00,'2016-09-05 00:08:41');
/*!40000 ALTER TABLE `current_driver_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driver_location`
--

DROP TABLE IF EXISTS `driver_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(8,5) NOT NULL,
  `latitude` decimal(8,5) NOT NULL,
  `heading` double NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver_location`
--

LOCK TABLES `driver_location` WRITE;
/*!40000 ALTER TABLE `driver_location` DISABLE KEYS */;
INSERT INTO `driver_location` VALUES (1,'19','0000000019102','',5.00000,5.00000,0,'2016-09-02 18:41:11'),(2,'19','0000000019102','',5.00000,5.00000,0,'2016-09-02 18:41:41'),(3,'19','0000000019102','',5.00000,5.00000,0,'2016-09-02 18:44:09');
/*!40000 ALTER TABLE `driver_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_pos_polisi`
--

DROP TABLE IF EXISTS `jenis_pos_polisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_pos_polisi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_pos_polisi`
--

LOCK TABLES `jenis_pos_polisi` WRITE;
/*!40000 ALTER TABLE `jenis_pos_polisi` DISABLE KEYS */;
INSERT INTO `jenis_pos_polisi` VALUES (1,'Polres','2016-09-11 06:31:11','2016-09-11 06:31:11','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `jenis_pos_polisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_panic`
--

DROP TABLE IF EXISTS `log_panic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_panic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driver_id` int(10) unsigned NOT NULL,
  `lat` double(8,2) NOT NULL,
  `lng` double(8,2) NOT NULL,
  `panic_id` int(10) unsigned NOT NULL,
  `msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_panic`
--

LOCK TABLES `log_panic` WRITE;
/*!40000 ALTER TABLE `log_panic` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_panic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_07_30_154608_create_lokasi_truk',1),('2016_07_30_154643_create_trucks',1),('2016_07_30_154655_create_panic_reports',1),('2016_07_30_154706_create_panic_report_logs',1),('2016_07_30_154720_create_status',1),('2016_07_30_154734_create_users',1),('2016_07_30_154746_create_broadcast',1),('2016_07_30_171932_table_relations',1),('2016_08_07_072549_add_pos_polisi_table',1),('2016_08_07_074845_add_jenis_pos_polisi_table',1),('2016_08_07_143511_add_master_panic',1),('2016_08_07_143858_add_log_panic',1),('2016_08_13_130852_create_last_driver_location',1),('2016_08_13_132832_create_fact_driver_location',1),('2016_08_13_152405_create_gcm_users',1),('2016_08_30_231728_add_api_session',1),('2016_08_30_232947_new_schema_driver_location',1),('2016_08_30_233556_change_schema_panic_report',1),('2016_08_30_233823_change_schema_broadcast',1),('2016_08_31_141702_add_role_to_users_table',1),('2016_08_31_145651_change_push_notification',1),('2016_08_31_153645_add_current_driver_location',1),('2016_09_02_031847_add_location_properties',1),('2016_09_02_033905_add_broadcast_range',1),('2016_09_02_165444_add_panic_report_response',1),('2016_09_02_170052_alter_panic_report',1),('2016_09_05_084526_add_password_to_users',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panic`
--

DROP TABLE IF EXISTS `panic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panic`
--

LOCK TABLES `panic` WRITE;
/*!40000 ALTER TABLE `panic` DISABLE KEYS */;
INSERT INTO `panic` VALUES (1,'Panic tingkat biasa','masih biasa',1,'2016-09-17 20:26:17','2016-09-17 20:26:21'),(2,'Test','test deskrips',0,'2016-09-17 14:17:00','2016-09-17 14:20:16'),(5,'Anime','Anime',0,'2016-09-17 16:14:07','2016-09-17 16:14:07'),(6,'Actizve','CreatActizve',1,'2016-09-17 16:15:44','2016-09-17 16:15:44'),(7,'mau apa ?','mau apa ?',1,'2016-09-17 16:17:43','2016-09-17 16:17:43');
/*!40000 ALTER TABLE `panic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panic_report`
--

DROP TABLE IF EXISTS `panic_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panic_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `panic_type_id` int(11) NOT NULL,
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `truck_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` decimal(8,5) NOT NULL,
  `latitude` decimal(8,5) NOT NULL,
  `message` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OPEN','RESPONDED','CLOSED') COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panic_report`
--

LOCK TABLES `panic_report` WRITE;
/*!40000 ALTER TABLE `panic_report` DISABLE KEYS */;
INSERT INTO `panic_report` VALUES (1,1,'19','','',5.00000,5.00000,'Brekele happened on street yoo!','OPEN','2016-09-02 18:31:36'),(2,1,'19','0000000019102','',5.00000,5.00000,'Brekele happened on street yoo!','OPEN','2016-09-02 18:32:27');
/*!40000 ALTER TABLE `panic_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panic_report_response`
--

DROP TABLE IF EXISTS `panic_report_response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panic_report_response` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `panic_report_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `response` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panic_report_response`
--

LOCK TABLES `panic_report_response` WRITE;
/*!40000 ALTER TABLE `panic_report_response` DISABLE KEYS */;
/*!40000 ALTER TABLE `panic_report_response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_polisi`
--

DROP TABLE IF EXISTS `pos_polisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_polisi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` double(8,5) NOT NULL,
  `long` double(8,5) NOT NULL,
  `jenis_pos_polisi_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `alamat` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_polisi`
--

LOCK TABLES `pos_polisi` WRITE;
/*!40000 ALTER TABLE `pos_polisi` DISABLE KEYS */;
INSERT INTO `pos_polisi` VALUES (1,'Polres Jakut',-6.12000,106.89000,1,0,'Kebon Bawang','Let kol Sugiono','2016-09-11 06:32:44','2016-09-11 06:32:44','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `pos_polisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_device`
--

DROP TABLE IF EXISTS `user_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_device` (
  `driver_id` char(13) COLLATE utf8_unicode_ci NOT NULL,
  `device_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_device`
--

LOCK TABLES `user_device` WRITE;
/*!40000 ALTER TABLE `user_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'admin','email@email.com','082124217369','admin','$2y$10$jJG9HQT5OaTVg9vXI4IX7.IBBYkLUwABrUkOd6D7Q/jDLEN1zO94q','oM8EXsdMKlqiEi5bLBLfvfdVz4ZqoKUUsP1bzrLPm5wGqNRTU9pxoi482pt7'),(7,'user1','user@email.com','0192837','staff','$2y$10$8M1XemSxq81syETV/p50SeK/7fiFAlTfupqhmh8ejnr14LhSHrnHa','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-03 18:06:14
